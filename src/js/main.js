const DeviceDetection = require("./components/device-detection");
const Helpers = require("./components/helpers");
const Popups = require("./components/popups");
const Carousel = require("./components/carousel");
//const Animation = require("./components/animation");
const Selects = require("./components/selects");
const Tabs = require("./components/tabs");
const Accordion = require("./components/accordion");

$(document).ready(function(){

  DeviceDetection.run();
  Selects.init();
  Helpers.init();
  Popups.init();
  Carousel.init();
  Tabs.init();
  Accordion.init();
  //Animation.init();
});


/**
 * Список экспортируемых модулей, чтобы иметь к ним доступ извне
 * @example
 * Main.Form.isFormValid();
 */
module.exports = {
  DeviceDetection,
  Helpers,
  Popups,
  Carousel,
  Selects,
  Tabs,
  Accordion,
  //Animation
};
