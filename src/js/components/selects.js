/**
 * Подключение Selects
 * @module Selects
 * https://select2.org/configuration/options-api
 */

function init() {

  if ($('.js-select').length) {
    $('.js-select').select2({
      width: 'style',
      minimumResultsForSearch: -1
    });
  }
}

module.exports = {
  init
}
