/**
 * Карусель
 * @module Carousel
 * https://idangero.us/swiper/api/
 */

/**
 * Инициализация карусели
 */
function init(){
    if ($(window).width() < 768) {
      
      if ($('.steps').length) {
        $('.steps').addClass('swiper-wrapper');

        var SwiperCarousel = new Swiper('.steps-slider', {
          slidesPerView: 1,
          navigation: {
            nextEl: '.steps-slider .swiper-button-next',
            prevEl: '.steps-slider .swiper-button-prev',
          },
          pagination: {
            el: '.steps-slider .swiper-pagination',
            clickable: true
          }
        });
      }

      if ($('.rules-prizes').length) {
        $('.rules-prizes').addClass('swiper-wrapper');

        var SwiperCarousel = new Swiper('.rules-slider', {
          slidesPerView: 1,
          navigation: {
            nextEl: '.rules-slider .swiper-button-next',
            prevEl: '.rules-slider .swiper-button-prev',
          },
          pagination: {
            el: '.rules-slider .swiper-pagination',
            clickable: true
          }
        });
      }
    }
};

module.exports = {init};
