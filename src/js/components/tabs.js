/**
 * инициализация событий для кастомного селекта
 * @example
 * Tabs.init();
 */

function init(){

  $('.tabs').each(function() {
    let active = $(this).find('.tab-ctrl.active').length ? $(this).find('.tab-ctrl.active') : $(this).find('.tab-ctrl').eq(0);
    active.addClass('active');
    $(active.data('tab')).fadeIn(500).addClass('active');
  })

  $('.tab-ctrl').on('click', function(){
    if (!$(this).hasClass('active')) {
      $(this).siblings('.tab-ctrl').removeClass('active');
      $(this).closest('.tabs').find('.tab.active').fadeOut(50).removeClass('active');
      $(this).addClass('active');
      $($(this).data('tab')).fadeIn(500).addClass('active');
    }
  });

  $('.btn-tab-ctrl').on('click', function() {
    let active = $(this).data('tab');
    $(this).closest('.tabs').find('.tab-ctrl').removeClass('active');
    $(this).closest('.tabs').find('.tab.active').fadeOut(50).removeClass('active');
    $(this).closest('.tabs').find(`.tab-ctrl[data-tab="${active}"]`).addClass('active');
    $(active).fadeIn(500).addClass('active');
  });

  $('.tab-prev').on('click', function() {
    if ($(this).closest('.tabs').find('.tab-ctrl.active').prev().length){
      $(this).closest('.tabs').find('.tab-ctrl.active').prev().trigger('click');
    } else {
      $(this).closest('.tabs').find('.tab-ctrl').last().trigger('click');
    }
  });
  $('.tab-next').on('click', function() {
    if ($(this).closest('.tabs').find('.tab-ctrl.active').next().length){
      $(this).closest('.tabs').find('.tab-ctrl.active').next().trigger('click');
    } else {
      $(this).closest('.tabs').find('.tab-ctrl').first().trigger('click');
    }
  });
}

module.exports = {init};
