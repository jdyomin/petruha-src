/**
 * Helpers
 * @module Helpers
 */

// Add script asynh
function addScript (url) {
  var tag = document.createElement("script");
  tag.src = url;
  var firstScriptTag = document.getElementsByTagName("script")[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

(function() {

  // проверяем поддержку
  if (!Element.prototype.closest) {

    // реализуем
    Element.prototype.closest = function(css) {
      var node = this;

      while (node) {
        if (node.matches(css)) return node;
        else node = node.parentElement;
      }
      return null;
    };
  }

})();

(function() {

  // проверяем поддержку
  if (!Element.prototype.matches) {

    // определяем свойство
    Element.prototype.matches = Element.prototype.matchesSelector ||
      Element.prototype.webkitMatchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector;

  }

})();

/**
 * Calculate scrollbar width in element
 * - if the width is 0 it means the scrollbar is floated/overlayed
 * - accepts "container" paremeter because ie & edge can have different
 *   scrollbar behaviors for different elements using '-ms-overflow-style'
 */
function getNativeScrollbarWidth (container) {
  container = container || document.body;

  let fullWidth = 0;
  let barWidth = 0;

  let wrapper = document.createElement('div');
  let child = document.createElement('div');

  wrapper.style.position = 'absolute';
  wrapper.style.pointerEvents = 'none';
  wrapper.style.bottom = '0';
  wrapper.style.right = '0';
  wrapper.style.width = '100px';
  wrapper.style.overflow = 'hidden';

  wrapper.appendChild(child);
  container.appendChild(wrapper);

  fullWidth = child.offsetWidth;
  wrapper.style.overflowY = 'scroll';
  barWidth = fullWidth - child.offsetWidth;

  container.removeChild(wrapper);

  return barWidth;
}

/**
 * Throttle Helper
 * https://remysharp.com/2010/07/21/throttling-function-calls
 */
function throttle (fn, threshhold, scope) {
  threshhold || (threshhold = 250);
  let last,
    deferTimer;
  return function () {
    let context = scope || this;

    let now = +new Date(),
      args = arguments;
    if (last && now < last + threshhold) {
      // hold on to it
      clearTimeout(deferTimer);
      deferTimer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshhold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  }
}

/**
 * Debounce Helper
 * https://remysharp.com/2010/07/21/throttling-function-calls
 */
function debounce (fn, delay) {
  let timer = null;
  return function () {
    let context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      fn.apply(context, args);
    }, delay);
  };
};

let timer;
let timeout = false;
let delta = 200;
function resizeEnd() {
  if (new Date() - timer < delta) {
    setTimeout(resizeEnd, delta);
  } else {
    timeout = false;
    $(window).trigger('resizeend');
  }
}

function toggleClassIf(el, cond, toggledClass){
	if(cond){
		el.addClass(toggledClass);
	} else {
		el.removeClass(toggledClass);
	}
}

/**
 * Функция добавляет к элементу класс, если страница прокручена больше, чем на указанное значение,
 * и убирает класс, если значение меньше
 * @param {object} el - элемент, с которым взаимодействуем
 * @param {mixed} [scrollValue=0] - значение прокрутки, на котором меняем css-класс, ожидаемое значение - число или ключевое слово 'this'. Если передано 'this', подставляется положение el.offset().top минус половина высоты экрана
 * @param {string} [toggledClass=scrolled] - css-класс, который переключаем
 */
function toggleElementClassOnScroll(el, scrollValue = 0, toggledClass = 'scrolled'){
	if(el.length == 0) {
		//console.error("Необходимо передать объект, с которым вы хотите взаимодействовать");
		return false;
	}

	if(scrollValue == 'this') {
		scrollValue = el.offset().top - $(window).outerHeight() / 2;
	}

	$(window).on('scroll', function(e){
		if($(window).scrollTop() > scrollValue){
			el.addClass(toggledClass);
		} else {
			el.removeClass(toggledClass);
		}
	});
};

/* Modals */
function openModal(modal) {
  if (modal) {
    let win = modal.find('.modal__window');
    modal.fadeIn(500);
    $('html, body').css('overflow-y', 'hidden');
    win.fadeIn(500);
    modal.trigger('modalopened');
  } else {
    console.error('Which modal?');
  }
}

function closeModal(modal) {
  if (modal) {
    let win = modal.find('.modal__window');
    win.fadeOut(500);
    modal.fadeOut(500);
    $('html, body').css('overflow-y', '');
    modal.trigger('modalclosed')
  } else {
    console.error('Which modal?');
  }
}

function setScrollpad(els) {
  if ($('.layout').outerHeight() > window.outerHeight || $('.page').hasClass('page--home')) {
    els.css({'padding-right': getNativeScrollbarWidth() + 'px'});
  } else {
    els.css({'padding-right': '0px'});
  }
}

/* Menu */
function showMenu() {
  $('.btn-menu').addClass('opened');
  $('.main-menu').addClass('opened');
  $('body').addClass('menu-opened');
}
function hideMenu() {
  $('.main-menu').removeClass('opened');
  $('body').removeClass('menu-opened');
  $('.btn-menu').removeClass('opened');
}

function textCounter(field, field2, maxlimit) {
  var countfield = document.querySelector(field2);
  if ( field.value.length > maxlimit ) {
    field.value = field.value.substring( 0, maxlimit );
    return false;
  } else {
    countfield.innerHTML = maxlimit - field.value.length;
  }
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function readURL(input) {
  if (input.files && input.files[0]) {
    var filesAmount = input.files.length;

    for (let i = 0; i < filesAmount; i++) {
      var reader = new FileReader();

      reader.onload = function(e) {
        var label = $('.js-img-add').parent();
        var imgBlock = '<div class="field__preview" style="background: url('+e.target.result+'); background-size: cover;"></div>';
        label.before($(imgBlock));
      }

      reader.readAsDataURL(input.files[i]);
    }
  }
}


/**
 * инициализация событий для переключателей классов
 * @example
 * Helpers.init();
 */
function init(){
  $(".js-img-add").change(function() {
    readURL(this);
  });

  //Inputmask().mask('input[name="birthday"]');

  // let textareaEl = '.js-form #send_letter-popup-letter';
  // $(textareaEl).on('input', function () {
  //   textCounter(document.querySelector(textareaEl), '.js-symbol-left', 1000);
  // });

  toggleElementClassOnScroll($('.header'), 50);

  $('.btn-menu').on('click', function(e){
    e.preventDefault();
    if ($(this).hasClass('opened')) {
      hideMenu();
    } else {
      showMenu();
    }
  });

  $('.js-toggle-block').on('click', function(){
    let target = $(this).data('target') === 'self' ? $(this).parent() : $($(this).data('target'));
    if (target.hasClass('off')) {
      target.removeClass('off').fadeIn(500);
    } else {
      target.addClass('off').fadeOut(500);
    }
  });

  let hoverToggleEvent = 'mouseenter mouseleave';
  if (Main.DeviceDetection.isTouch()) {
    hoverToggleEvent = 'click';
  }
  $('.js-toggle-hover-block').on(hoverToggleEvent, function(){
    let target = $(this).data('target') === 'self' ? $(this).parent() : $($(this).data('target'));
    if (target.hasClass('off')) {
      target.removeClass('off').fadeIn(500);
    } else {
      if (hoverToggleEvent === 'click') {
        target.addClass('off').fadeOut(500);
      } else {
        target.on('mouseleave', function() {
          target.addClass('off').fadeOut(500);
        });
      }
    }
  });

  $(document).on('click', '.btn-close-modal', function(){
    let modal = !!$(this).data('target') ? $($(this).data('target')) : $(this).closest('.modal');
    closeModal(modal);
  });

  $(document).on('click', '.modal', function() {
    closeModal($(this));
  });

  $(document).on('click', '.modal__window', function(e) {
    e.stopPropagation();
  });

  $(document).on('click', '.btn-modal', function(e) {
    let target = $(this).data('target') === 'self' ? $(this).parent() : $($(this).data('target'));
    e.preventDefault();
    openModal(target);
  });

  $(document).on('click', '.js-letter-share', function (e) {
    e.preventDefault();
    $(this).parent().parent().toggleClass('_share');
  });

  // $(window).on('resize', function () {
  //   timer = new Date();
  //   if (timeout === false) {
  //     timeout = true;
  //     setTimeout(resizeEnd, delta);
  //   }
  // });

  //setScrollpad($('.layout, .header'));

  // $(window).on('resizeend', function(){
  //   setScrollpad($('.layout, .header'));
  // });

  // $(window).on('resizeend', function(){
  //   if (Main.DeviceDetection.isMobile()) {
  //     hideMenu();
  //   } else {
  //     showMenu();
  //   }
  // });

  /*
  if (Main.DeviceDetection.isPortrait()) {
    $('html').addClass('rotated');
    $('.rotate').fadeIn(500);
  }

  $(window).on('resizeend', function(){
    if (Main.DeviceDetection.isPortrait()) {
      $('html').addClass('rotated');
      $('.rotate').fadeIn(500);
    } else {
      $('.rotate').fadeOut(500);
      $('html').removeClass('rotated');
    }
  });
  */

  $('.btn-accordion-menu').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.multi').toggleClass('open');
    $(this).siblings('.subnav').slideToggle(500);
  })

  $('.feedback').find('input[type="file"]').on('change', function () {
    let fi = $(this).get(0);
    let form = $(this).parents('form');
    if (fi.files.length > 0) {
      for (let i = 0; i <= fi.files.length - 1; i++) {
        var fullPath = $(this).val();
        var pathEl = $('.js-file-path');
        if (fullPath) {
            var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }
            $(this).parents('.field').addClass('_show-path');
            pathEl.html(filename);
        }
      }
    }
  });

  if ($(window).width() > 1199) {
    $('.js-submenu').on('click', function() {
      $(this).toggleClass('opened');
    });
    $(document).on('click', function(e) {
      if (!$(e.target).is('.main-menu__name, .main-menu__avatar, .js-submenu')) {
        $('.js-submenu').removeClass('opened');
      }
    });
  }

  // $(window).scroll($.debounce(250, true, function() {
  //   $('html').addClass('is-scrolling');
  // }));
  // $(window).scroll($.debounce(250, function() {
  //   $('html').removeClass('is-scrolling');
  // }));

}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function trimText(el) {
  var wrap = $('[data-target="'+el+'"]').parents('.letter--text');
  var p = wrap.find('.js-letter-preview-text-p');
  var divh = wrap.find('.js-letter-preview-text').height();

  if (p.outerHeight() > divh) {
      p.text(function (index, text) {
          return text.replace(/\W*\s(\S)*$/, '...');
      });
  }
}

function zeroPad(nr,base){
  var  len = (String(base).length - String(nr).length)+1;
  return len > 0? new Array(len).join('0')+nr : nr;
}

module.exports = {
  init,
  getNativeScrollbarWidth,
  toggleClassIf,
  toggleElementClassOnScroll,
  addScript,
  openModal,
  closeModal,
  showMenu,
  hideMenu,
  zeroPad,
  trimText,
  getParameterByName,
  isEmail
};
