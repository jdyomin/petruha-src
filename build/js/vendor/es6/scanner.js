import QrScanner from './qr-scanner.min.js';
QrScanner.WORKER_PATH = './build/js/vendor/es6/qr-scanner-worker.min.js';

const video = document.getElementById('qr-video');
var scannerBtn = document.querySelectorAll('.js-scanner-btn');
const scanner = new QrScanner(video, result => setResult(result));

function setResult(result) {
  console.log(result);
  scanner.pause();
  sendResult($('.js-scan-form'), result);
}

// function setFileQrResult(fileQrResult, result) {
//   console.log(result);
//   if (fileQrResult) {
//     document.getElementById('qr_input_result').value = result;
//     document.querySelector('.js-qr-upload-btn').disabled = false;
//   } else {
//     closeModal('receipt2');
//     openModal('receipt6');
//     document.getElementById('upload2').classList.remove('file-uploaded');
//     document.querySelector('.js-qr-upload-btn').disabled = true;
//   }
// }

Array.from(scannerBtn).forEach(function(element) {
  element.addEventListener('click', function(e) {
    e.preventDefault();
    scanner.start();
  });
});

$(window).on('scan-stop', function(){
  scanner.stop();
});

// document.getElementById('qr_input').addEventListener('change', event => {
//   const file = document.getElementById('qr_input').files[0];
//   if (!file) {
//     return;
//   }
//   QrScanner.scanImage(file)
//     .then(result => setFileQrResult(true, result))
//     .catch(e => setFileQrResult(false, e));
// });

// $('.js-qr-upload-btn').on('click', function(e) {
//   sendUploadResult($('.js-upload-form'));
// });

function sendResult(form, result) {
  var request = $.ajax({
    url: form.attr('action'),
    type: form.attr('method'),
    data: {code: result},
    dataType: 'json'
  });
  request.done(function (response, textStatus, jqXHR) {
    // if (response.success) {
    //   closeModal('receipt2');
    //   openModal('receipt5');
    // } else {
    //   closeModal('receipt2');
    //   openModal('receipt6');
    // }
    Main.Popups.closePopup('#qr-popup');
  });
  request.fail(function (jqXHR, textStatus) {
    //closeModal('receipt2');
    //openModal('receipt6');
    console.log("Request failed: " + textStatus);
    Main.Popups.closePopup('#qr-popup');
  });
}

// function sendUploadResult(form, result) {
//   var formData = new FormData(form[0]);
//   var request = $.ajax({
//     url: form.attr('action'),
//     type: form.attr('method'),
//     data: formData,
//     cache:false,
//     contentType: false,
//     processData: false,
//     beforeSend: function () {
//       $('.js-qr-upload-btn').attr('disabled', 'disabled');
//     }
//   });
//   request.done(function (response, textStatus, jqXHR) {
//     if (response.success) {
//       closeModal('receipt2');
//       openModal('receipt5');
//     } else {
//       closeModal('receipt2');
//       openModal('receipt6');
//     }
//     $('.js-qr-upload-btn').attr('disabled', 'false');
//   });
//   request.fail(function (jqXHR, textStatus) {
//     closeModal('receipt2');
//     openModal('receipt6');
//     console.log("Request failed: " + textStatus);
//     $('.js-qr-upload-btn').attr('disabled', 'false');
//   });
// }
