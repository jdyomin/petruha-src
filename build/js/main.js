var Main =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/kenzo/build/js/";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	var DeviceDetection = __webpack_require__(1);
	var Helpers = __webpack_require__(2);
	var Popups = __webpack_require__(3);
	var Carousel = __webpack_require__(4);
	//const Animation = require("./components/animation");
	var Selects = __webpack_require__(5);
	var Tabs = __webpack_require__(6);
	var Accordion = __webpack_require__(7);

	$(document).ready(function () {

	  DeviceDetection.run();
	  Selects.init();
	  Helpers.init();
	  Popups.init();
	  Carousel.init();
	  Tabs.init();
	  Accordion.init();
	  //Animation.init();
	});

	/**
	 * Список экспортируемых модулей, чтобы иметь к ним доступ извне
	 * @example
	 * Main.Form.isFormValid();
	 */
	module.exports = {
	  DeviceDetection: DeviceDetection,
	  Helpers: Helpers,
	  Popups: Popups,
	  Carousel: Carousel,
	  Selects: Selects,
	  Tabs: Tabs,
	  Accordion: Accordion
	  //Animation
		};

/***/ }),
/* 1 */
/***/ (function(module, exports) {

	'use strict';

	var breakpoints = {
	  ms: 480,
	  sm: 768,
	  md: 1024,
	  lg: 1280,
	  xl: 1600
	};

	function isPortrait() {
	  return $(window).width() < $(window).height();
	}
	function isLandscape() {
	  return $(window).width() > $(window).height();
	}
	function isMobile() {
	  return $(window).width() <= breakpoints.sm;
	}
	function isMobileMS() {
	  return $(window).width() <= breakpoints.ms;
	}
	function isTablet() {
	  return $(window).width() > breakpoints.sm && $(window).width() <= breakpoints.md;
	}
	function isDesktopExt() {
	  return $(window).width() >= breakpoints.md;
	}
	function isDesktop() {
	  return $(window).width() > breakpoints.md;
	}
	function isTouch() {
	  return 'ontouchstart' in window || navigator.maxTouchPoints;
	}
	function isMobileVersion() {
	  return !!~window.location.href.indexOf("/mobile/");
	}

	function run() {
	  if (isTouch()) {
	    $('html').removeClass('no-touch').addClass('touch');
	  } else {
	    $('html').removeClass('touch').addClass('no-touch');
	  }
	}

	module.exports = {
	  run: run,
	  isTouch: isTouch,
	  isMobile: isMobile,
	  isMobileMS: isMobileMS,
	  isTablet: isTablet,
	  isDesktop: isDesktop,
	  isDesktopExt: isDesktopExt,
	  isMobileVersion: isMobileVersion,
	  isPortrait: isPortrait,
	  isLandscape: isLandscape
		};

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	"use strict";

	/**
	 * Helpers
	 * @module Helpers
	 */

	// Add script asynh
	function addScript(url) {
	  var tag = document.createElement("script");
	  tag.src = url;
	  var firstScriptTag = document.getElementsByTagName("script")[0];
	  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	}

	(function () {

	  // проверяем поддержку
	  if (!Element.prototype.closest) {

	    // реализуем
	    Element.prototype.closest = function (css) {
	      var node = this;

	      while (node) {
	        if (node.matches(css)) return node;else node = node.parentElement;
	      }
	      return null;
	    };
	  }
	})();

	(function () {

	  // проверяем поддержку
	  if (!Element.prototype.matches) {

	    // определяем свойство
	    Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.webkitMatchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector;
	  }
	})();

	/**
	 * Calculate scrollbar width in element
	 * - if the width is 0 it means the scrollbar is floated/overlayed
	 * - accepts "container" paremeter because ie & edge can have different
	 *   scrollbar behaviors for different elements using '-ms-overflow-style'
	 */
	function getNativeScrollbarWidth(container) {
	  container = container || document.body;

	  var fullWidth = 0;
	  var barWidth = 0;

	  var wrapper = document.createElement('div');
	  var child = document.createElement('div');

	  wrapper.style.position = 'absolute';
	  wrapper.style.pointerEvents = 'none';
	  wrapper.style.bottom = '0';
	  wrapper.style.right = '0';
	  wrapper.style.width = '100px';
	  wrapper.style.overflow = 'hidden';

	  wrapper.appendChild(child);
	  container.appendChild(wrapper);

	  fullWidth = child.offsetWidth;
	  wrapper.style.overflowY = 'scroll';
	  barWidth = fullWidth - child.offsetWidth;

	  container.removeChild(wrapper);

	  return barWidth;
	}

	/**
	 * Throttle Helper
	 * https://remysharp.com/2010/07/21/throttling-function-calls
	 */
	function throttle(fn, threshhold, scope) {
	  threshhold || (threshhold = 250);
	  var last = void 0,
	      deferTimer = void 0;
	  return function () {
	    var context = scope || this;

	    var now = +new Date(),
	        args = arguments;
	    if (last && now < last + threshhold) {
	      // hold on to it
	      clearTimeout(deferTimer);
	      deferTimer = setTimeout(function () {
	        last = now;
	        fn.apply(context, args);
	      }, threshhold);
	    } else {
	      last = now;
	      fn.apply(context, args);
	    }
	  };
	}

	/**
	 * Debounce Helper
	 * https://remysharp.com/2010/07/21/throttling-function-calls
	 */
	function debounce(fn, delay) {
	  var timer = null;
	  return function () {
	    var context = this,
	        args = arguments;
	    clearTimeout(timer);
	    timer = setTimeout(function () {
	      fn.apply(context, args);
	    }, delay);
	  };
	};

	var timer = void 0;
	var timeout = false;
	var delta = 200;
	function resizeEnd() {
	  if (new Date() - timer < delta) {
	    setTimeout(resizeEnd, delta);
	  } else {
	    timeout = false;
	    $(window).trigger('resizeend');
	  }
	}

	function toggleClassIf(el, cond, toggledClass) {
	  if (cond) {
	    el.addClass(toggledClass);
	  } else {
	    el.removeClass(toggledClass);
	  }
	}

	/**
	 * Функция добавляет к элементу класс, если страница прокручена больше, чем на указанное значение,
	 * и убирает класс, если значение меньше
	 * @param {object} el - элемент, с которым взаимодействуем
	 * @param {mixed} [scrollValue=0] - значение прокрутки, на котором меняем css-класс, ожидаемое значение - число или ключевое слово 'this'. Если передано 'this', подставляется положение el.offset().top минус половина высоты экрана
	 * @param {string} [toggledClass=scrolled] - css-класс, который переключаем
	 */
	function toggleElementClassOnScroll(el) {
	  var scrollValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
	  var toggledClass = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'scrolled';

	  if (el.length == 0) {
	    //console.error("Необходимо передать объект, с которым вы хотите взаимодействовать");
	    return false;
	  }

	  if (scrollValue == 'this') {
	    scrollValue = el.offset().top - $(window).outerHeight() / 2;
	  }

	  $(window).on('scroll', function (e) {
	    if ($(window).scrollTop() > scrollValue) {
	      el.addClass(toggledClass);
	    } else {
	      el.removeClass(toggledClass);
	    }
	  });
	};

	/* Modals */
	function openModal(modal) {
	  if (modal) {
	    var win = modal.find('.modal__window');
	    modal.fadeIn(500);
	    $('html, body').css('overflow-y', 'hidden');
	    win.fadeIn(500);
	    modal.trigger('modalopened');
	  } else {
	    console.error('Which modal?');
	  }
	}

	function closeModal(modal) {
	  if (modal) {
	    var win = modal.find('.modal__window');
	    win.fadeOut(500);
	    modal.fadeOut(500);
	    $('html, body').css('overflow-y', '');
	    modal.trigger('modalclosed');
	  } else {
	    console.error('Which modal?');
	  }
	}

	function setScrollpad(els) {
	  if ($('.layout').outerHeight() > window.outerHeight || $('.page').hasClass('page--home')) {
	    els.css({ 'padding-right': getNativeScrollbarWidth() + 'px' });
	  } else {
	    els.css({ 'padding-right': '0px' });
	  }
	}

	/* Menu */
	function showMenu() {
	  $('.btn-menu').addClass('opened');
	  $('.main-menu').addClass('opened');
	  $('body').addClass('menu-opened');
	}
	function hideMenu() {
	  $('.main-menu').removeClass('opened');
	  $('body').removeClass('menu-opened');
	  $('.btn-menu').removeClass('opened');
	}

	function textCounter(field, field2, maxlimit) {
	  var countfield = document.querySelector(field2);
	  if (field.value.length > maxlimit) {
	    field.value = field.value.substring(0, maxlimit);
	    return false;
	  } else {
	    countfield.innerHTML = maxlimit - field.value.length;
	  }
	}

	function isEmail(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}

	function readURL(input) {
	  if (input.files && input.files[0]) {
	    var filesAmount = input.files.length;

	    for (var i = 0; i < filesAmount; i++) {
	      var reader = new FileReader();

	      reader.onload = function (e) {
	        var label = $('.js-img-add').parent();
	        var imgBlock = '<div class="field__preview" style="background: url(' + e.target.result + '); background-size: cover;"></div>';
	        label.before($(imgBlock));
	      };

	      reader.readAsDataURL(input.files[i]);
	    }
	  }
	}

	/**
	 * инициализация событий для переключателей классов
	 * @example
	 * Helpers.init();
	 */
	function init() {
	  $(".js-img-add").change(function () {
	    readURL(this);
	  });

	  //Inputmask().mask('input[name="birthday"]');

	  // let textareaEl = '.js-form #send_letter-popup-letter';
	  // $(textareaEl).on('input', function () {
	  //   textCounter(document.querySelector(textareaEl), '.js-symbol-left', 1000);
	  // });

	  toggleElementClassOnScroll($('.header'), 50);

	  $('.btn-menu').on('click', function (e) {
	    e.preventDefault();
	    if ($(this).hasClass('opened')) {
	      hideMenu();
	    } else {
	      showMenu();
	    }
	  });

	  $('.js-toggle-block').on('click', function () {
	    var target = $(this).data('target') === 'self' ? $(this).parent() : $($(this).data('target'));
	    if (target.hasClass('off')) {
	      target.removeClass('off').fadeIn(500);
	    } else {
	      target.addClass('off').fadeOut(500);
	    }
	  });

	  var hoverToggleEvent = 'mouseenter mouseleave';
	  if (Main.DeviceDetection.isTouch()) {
	    hoverToggleEvent = 'click';
	  }
	  $('.js-toggle-hover-block').on(hoverToggleEvent, function () {
	    var target = $(this).data('target') === 'self' ? $(this).parent() : $($(this).data('target'));
	    if (target.hasClass('off')) {
	      target.removeClass('off').fadeIn(500);
	    } else {
	      if (hoverToggleEvent === 'click') {
	        target.addClass('off').fadeOut(500);
	      } else {
	        target.on('mouseleave', function () {
	          target.addClass('off').fadeOut(500);
	        });
	      }
	    }
	  });

	  $(document).on('click', '.btn-close-modal', function () {
	    var modal = !!$(this).data('target') ? $($(this).data('target')) : $(this).closest('.modal');
	    closeModal(modal);
	  });

	  $(document).on('click', '.modal', function () {
	    closeModal($(this));
	  });

	  $(document).on('click', '.modal__window', function (e) {
	    e.stopPropagation();
	  });

	  $(document).on('click', '.btn-modal', function (e) {
	    var target = $(this).data('target') === 'self' ? $(this).parent() : $($(this).data('target'));
	    e.preventDefault();
	    openModal(target);
	  });

	  $(document).on('click', '.js-letter-share', function (e) {
	    e.preventDefault();
	    $(this).parent().parent().toggleClass('_share');
	  });

	  // $(window).on('resize', function () {
	  //   timer = new Date();
	  //   if (timeout === false) {
	  //     timeout = true;
	  //     setTimeout(resizeEnd, delta);
	  //   }
	  // });

	  //setScrollpad($('.layout, .header'));

	  // $(window).on('resizeend', function(){
	  //   setScrollpad($('.layout, .header'));
	  // });

	  // $(window).on('resizeend', function(){
	  //   if (Main.DeviceDetection.isMobile()) {
	  //     hideMenu();
	  //   } else {
	  //     showMenu();
	  //   }
	  // });

	  /*
	  if (Main.DeviceDetection.isPortrait()) {
	    $('html').addClass('rotated');
	    $('.rotate').fadeIn(500);
	  }
	    $(window).on('resizeend', function(){
	    if (Main.DeviceDetection.isPortrait()) {
	      $('html').addClass('rotated');
	      $('.rotate').fadeIn(500);
	    } else {
	      $('.rotate').fadeOut(500);
	      $('html').removeClass('rotated');
	    }
	  });
	  */

	  $('.btn-accordion-menu').on('click', function (e) {
	    e.preventDefault();
	    $(this).closest('.multi').toggleClass('open');
	    $(this).siblings('.subnav').slideToggle(500);
	  });

	  $('.feedback').find('input[type="file"]').on('change', function () {
	    var fi = $(this).get(0);
	    var form = $(this).parents('form');
	    if (fi.files.length > 0) {
	      for (var i = 0; i <= fi.files.length - 1; i++) {
	        var fullPath = $(this).val();
	        var pathEl = $('.js-file-path');
	        if (fullPath) {
	          var startIndex = fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/');
	          var filename = fullPath.substring(startIndex);
	          if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
	            filename = filename.substring(1);
	          }
	          $(this).parents('.field').addClass('_show-path');
	          pathEl.html(filename);
	        }
	      }
	    }
	  });

	  if ($(window).width() > 1199) {
	    $('.js-submenu').on('click', function () {
	      $(this).toggleClass('opened');
	    });
	    $(document).on('click', function (e) {
	      if (!$(e.target).is('.main-menu__name, .main-menu__avatar, .js-submenu')) {
	        $('.js-submenu').removeClass('opened');
	      }
	    });
	  }

	  // $(window).scroll($.debounce(250, true, function() {
	  //   $('html').addClass('is-scrolling');
	  // }));
	  // $(window).scroll($.debounce(250, function() {
	  //   $('html').removeClass('is-scrolling');
	  // }));
	}

	function getParameterByName(name, url) {
	  if (!url) url = window.location.href;
	  name = name.replace(/[\[\]]/g, '\\$&');
	  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	      results = regex.exec(url);
	  if (!results) return null;
	  if (!results[2]) return '';
	  return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}

	function trimText(el) {
	  var wrap = $('[data-target="' + el + '"]').parents('.letter--text');
	  var p = wrap.find('.js-letter-preview-text-p');
	  var divh = wrap.find('.js-letter-preview-text').height();

	  if (p.outerHeight() > divh) {
	    p.text(function (index, text) {
	      return text.replace(/\W*\s(\S)*$/, '...');
	    });
	  }
	}

	function zeroPad(nr, base) {
	  var len = String(base).length - String(nr).length + 1;
	  return len > 0 ? new Array(len).join('0') + nr : nr;
	}

	module.exports = {
	  init: init,
	  getNativeScrollbarWidth: getNativeScrollbarWidth,
	  toggleClassIf: toggleClassIf,
	  toggleElementClassOnScroll: toggleElementClassOnScroll,
	  addScript: addScript,
	  openModal: openModal,
	  closeModal: closeModal,
	  showMenu: showMenu,
	  hideMenu: hideMenu,
	  zeroPad: zeroPad,
	  trimText: trimText,
	  getParameterByName: getParameterByName,
	  isEmail: isEmail
		};

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * Переключение классов по различным событиям
	 * @module Popups
	 */

	/* Popups */
	function openPopup(popupID) {
	  closeAllPopups();
	  if (popupID) {
	    var popup = $(popupID);
	    var win = popup.find('.popup__window');
	    popup.fadeIn(500);
	    if (Main.DeviceDetection.isPortrait()) {
	      $('html, body').css('overflow', 'hidden');
	    }
	    $('html').addClass('popup-opened');
	    win.fadeIn(500);
	    popup.trigger('popupopened');
	    $(window).trigger('popupopened');

	    if (popup.attr('id') == 'crop-popup') {
	      $(window).trigger('crop-popupopened');
	    }
	  } else {
	    console.error('Which popup?');
	  }
	}

	function closePopup(popupID) {
	  if (popupID) {
	    var popup = $(popupID);
	    var win = popup.find('.popup__window');
	    win.fadeOut(500);
	    popup.fadeOut(500);
	    $('html').removeClass('popup-opened');
	    if (Main.DeviceDetection.isPortrait()) {
	      $('html, body').css('overflow', 'visible');
	    }
	    popup.trigger('popupclosed');
	    $(window).trigger('popupclosed');
	  } else {
	    console.error('Which popup?');
	  }
	  if (popupID == '#qr-popup') {
	    $(window).trigger('scan-stop');
	  }
	  //Main.Helpers.removeHash();
	}

	function closeAllPopups() {
	  var popup = $('.popup');
	  var win = popup.find('.popup__window');
	  win.fadeOut(200);
	  popup.fadeOut(200);
	  $('html').removeClass('popup-opened');
	  if (Main.DeviceDetection.isPortrait()) {
	    $('html, body').css('overflow', 'visible');
	  }
	  popup.trigger('popupclosed');
	  $(window).trigger('popupclosed');
	  var event = new Event('scan-stop');
	  document.dispatchEvent(event);
	  //Main.Helpers.removeHash();
	}

	function init() {

	  $(document).on('click', '.btn-close-popup', function () {
	    var popup = !!$(this).data('target') ? $($(this).data('target')) : $(this).closest('.popup').attr('id');
	    closePopup('#' + popup);
	  });

	  $(document).on('click', '.popup', function () {
	    if (!$(this).hasClass('unclosed')) {
	      closePopup('#' + $(this).attr('id'));
	    }
	  });

	  $(document).on('click', '.popup__window, .popup .mCSB_scrollTools', function (e) {
	    e.stopPropagation();
	  });

	  $(document).on('click', '.btn-popup', function (e) {
	    var target = $(this).data('target') === 'self' ? $(this).parent() : $($(this).data('target'));
	    e.preventDefault();
	    openPopup(target);
	  });

	  $(window).on('popupclosed', function () {
	    $('video').trigger('pause');
	  });

	  // apm-widjet
	  // https://qr.apmcheck.ru/readme
	  if (!window.sys) {
	    window.sys = {};
	    window.sys.userUuid = "test@mail.ru";
	  }
	  var widgetParams = {
	    api: 'https://api.apmcheck.ru',
	    apiKey: '08fa945b-b911-45a6-babe-26e54f57b4cc',
	    userUuid: window.sys.userUuid,
	    i18n: {
	      labels: {
	        mainButton: 'Загрузить чек',
	        manualQrButton: 'Ввести данные вручную',
	        uploadPhotosButton: 'Загрузить фото чека',
	        submitManualQrButton: 'Отправить',
	        addPhotoButton: '<svg class="icon"><use xlink:href="#upload"/></svg> <span class="text">Загрузить фото</span>'
	      },
	      screens: {
	        cameraNotAvailable: {
	          header: "Загрузка чека",
	          subheader: 'Мы не&nbsp;можем получить доступ к&nbsp;камере&nbsp;устройства.<br><br/>Разрешите браузеру обращаться к&nbsp;камере или введите данные с&nbsp;чека вручную'
	        }
	      }
	    }
	  };

	  if ($('#apm-widget').length) {
	    qrWidget.init('apm-widget', widgetParams);
	    $(document).on('click', '.js-scanner-btn', function (ev) {
	      ev.preventDefault();
	      var e = document.createEvent('HTMLEvents');
	      e.initEvent('click', true, true);
	      $('#apm-scan-qr')[0].dispatchEvent(e);
	    });
	    $(document).on('click', '.apm-btn-link ', function (ev) {
	      ev.preventDefault();
	    });
	  }
	}

	module.exports = {
	  init: init,
	  openPopup: openPopup,
	  closePopup: closePopup,
	  closeAllPopups: closeAllPopups
		};

/***/ }),
/* 4 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * Карусель
	 * @module Carousel
	 * https://idangero.us/swiper/api/
	 */

	/**
	 * Инициализация карусели
	 */
	function init() {
	  if ($(window).width() < 768) {

	    if ($('.steps').length) {
	      $('.steps').addClass('swiper-wrapper');

	      var SwiperCarousel = new Swiper('.steps-slider', {
	        slidesPerView: 1,
	        navigation: {
	          nextEl: '.steps-slider .swiper-button-next',
	          prevEl: '.steps-slider .swiper-button-prev'
	        },
	        pagination: {
	          el: '.steps-slider .swiper-pagination',
	          clickable: true
	        }
	      });
	    }

	    if ($('.rules-prizes').length) {
	      $('.rules-prizes').addClass('swiper-wrapper');

	      var SwiperCarousel = new Swiper('.rules-slider', {
	        slidesPerView: 1,
	        navigation: {
	          nextEl: '.rules-slider .swiper-button-next',
	          prevEl: '.rules-slider .swiper-button-prev'
	        },
	        pagination: {
	          el: '.rules-slider .swiper-pagination',
	          clickable: true
	        }
	      });
	    }
	  }
	};

	module.exports = { init: init };

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * Подключение Selects
	 * @module Selects
	 * https://select2.org/configuration/options-api
	 */

	function init() {

	  if ($('.js-select').length) {
	    $('.js-select').select2({
	      width: 'style',
	      minimumResultsForSearch: -1
	    });
	  }
	}

	module.exports = {
	  init: init
		};

/***/ }),
/* 6 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * инициализация событий для кастомного селекта
	 * @example
	 * Tabs.init();
	 */

	function init() {

	  $('.tabs').each(function () {
	    var active = $(this).find('.tab-ctrl.active').length ? $(this).find('.tab-ctrl.active') : $(this).find('.tab-ctrl').eq(0);
	    active.addClass('active');
	    $(active.data('tab')).fadeIn(500).addClass('active');
	  });

	  $('.tab-ctrl').on('click', function () {
	    if (!$(this).hasClass('active')) {
	      $(this).siblings('.tab-ctrl').removeClass('active');
	      $(this).closest('.tabs').find('.tab.active').fadeOut(50).removeClass('active');
	      $(this).addClass('active');
	      $($(this).data('tab')).fadeIn(500).addClass('active');
	    }
	  });

	  $('.btn-tab-ctrl').on('click', function () {
	    var active = $(this).data('tab');
	    $(this).closest('.tabs').find('.tab-ctrl').removeClass('active');
	    $(this).closest('.tabs').find('.tab.active').fadeOut(50).removeClass('active');
	    $(this).closest('.tabs').find('.tab-ctrl[data-tab="' + active + '"]').addClass('active');
	    $(active).fadeIn(500).addClass('active');
	  });

	  $('.tab-prev').on('click', function () {
	    if ($(this).closest('.tabs').find('.tab-ctrl.active').prev().length) {
	      $(this).closest('.tabs').find('.tab-ctrl.active').prev().trigger('click');
	    } else {
	      $(this).closest('.tabs').find('.tab-ctrl').last().trigger('click');
	    }
	  });
	  $('.tab-next').on('click', function () {
	    if ($(this).closest('.tabs').find('.tab-ctrl.active').next().length) {
	      $(this).closest('.tabs').find('.tab-ctrl.active').next().trigger('click');
	    } else {
	      $(this).closest('.tabs').find('.tab-ctrl').first().trigger('click');
	    }
	  });
	}

	module.exports = { init: init };

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * инициализация событий для кастомного селекта
	 * @example
	 * Accordion.init();
	 */

	function init() {

	  $('.accordion__panel').on('click', function () {
	    $(this).closest('.accordion').siblings('.accordion').removeClass('active');
	    $(this).closest('.accordion').siblings('.accordion').find('.accordion__content').slideUp(500);
	    $(this).closest('.accordion').toggleClass('active');
	    $(this).siblings('.accordion__content').slideToggle(500);
	  });
	}

	module.exports = { init: init };

/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCA5MGQ0ZjhiYjUzY2VlNWMwYzUwNSIsIndlYnBhY2s6Ly8vc3JjL2pzL21haW4uanMiLCJ3ZWJwYWNrOi8vL3NyYy9qcy9jb21wb25lbnRzL2RldmljZS1kZXRlY3Rpb24uanMiLCJ3ZWJwYWNrOi8vL3NyYy9qcy9jb21wb25lbnRzL2hlbHBlcnMuanMiLCJ3ZWJwYWNrOi8vL3NyYy9qcy9jb21wb25lbnRzL3BvcHVwcy5qcyIsIndlYnBhY2s6Ly8vc3JjL2pzL2NvbXBvbmVudHMvY2Fyb3VzZWwuanMiLCJ3ZWJwYWNrOi8vL3NyYy9qcy9jb21wb25lbnRzL3NlbGVjdHMuanMiLCJ3ZWJwYWNrOi8vL3NyYy9qcy9jb21wb25lbnRzL3RhYnMuanMiLCJ3ZWJwYWNrOi8vL3NyYy9qcy9jb21wb25lbnRzL2FjY29yZGlvbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSlcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcblxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0ZXhwb3J0czoge30sXG4gXHRcdFx0aWQ6IG1vZHVsZUlkLFxuIFx0XHRcdGxvYWRlZDogZmFsc2VcbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubG9hZGVkID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCIva2Vuem8vYnVpbGQvanMvXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgOTBkNGY4YmI1M2NlZTVjMGM1MDUiLCJjb25zdCBEZXZpY2VEZXRlY3Rpb24gPSByZXF1aXJlKFwiLi9jb21wb25lbnRzL2RldmljZS1kZXRlY3Rpb25cIik7XHJcbmNvbnN0IEhlbHBlcnMgPSByZXF1aXJlKFwiLi9jb21wb25lbnRzL2hlbHBlcnNcIik7XHJcbmNvbnN0IFBvcHVwcyA9IHJlcXVpcmUoXCIuL2NvbXBvbmVudHMvcG9wdXBzXCIpO1xyXG5jb25zdCBDYXJvdXNlbCA9IHJlcXVpcmUoXCIuL2NvbXBvbmVudHMvY2Fyb3VzZWxcIik7XHJcbi8vY29uc3QgQW5pbWF0aW9uID0gcmVxdWlyZShcIi4vY29tcG9uZW50cy9hbmltYXRpb25cIik7XHJcbmNvbnN0IFNlbGVjdHMgPSByZXF1aXJlKFwiLi9jb21wb25lbnRzL3NlbGVjdHNcIik7XHJcbmNvbnN0IFRhYnMgPSByZXF1aXJlKFwiLi9jb21wb25lbnRzL3RhYnNcIik7XHJcbmNvbnN0IEFjY29yZGlvbiA9IHJlcXVpcmUoXCIuL2NvbXBvbmVudHMvYWNjb3JkaW9uXCIpO1xyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXtcclxuXHJcbiAgRGV2aWNlRGV0ZWN0aW9uLnJ1bigpO1xyXG4gIFNlbGVjdHMuaW5pdCgpO1xyXG4gIEhlbHBlcnMuaW5pdCgpO1xyXG4gIFBvcHVwcy5pbml0KCk7XHJcbiAgQ2Fyb3VzZWwuaW5pdCgpO1xyXG4gIFRhYnMuaW5pdCgpO1xyXG4gIEFjY29yZGlvbi5pbml0KCk7XHJcbiAgLy9BbmltYXRpb24uaW5pdCgpO1xyXG59KTtcclxuXHJcblxyXG4vKipcclxuICog0KHQv9C40YHQvtC6INGN0LrRgdC/0L7RgNGC0LjRgNGD0LXQvNGL0YUg0LzQvtC00YPQu9C10LksINGH0YLQvtCx0Ysg0LjQvNC10YLRjCDQuiDQvdC40Lwg0LTQvtGB0YLRg9C/INC40LfQstC90LVcclxuICogQGV4YW1wbGVcclxuICogTWFpbi5Gb3JtLmlzRm9ybVZhbGlkKCk7XHJcbiAqL1xyXG5tb2R1bGUuZXhwb3J0cyA9IHtcclxuICBEZXZpY2VEZXRlY3Rpb24sXHJcbiAgSGVscGVycyxcclxuICBQb3B1cHMsXHJcbiAgQ2Fyb3VzZWwsXHJcbiAgU2VsZWN0cyxcclxuICBUYWJzLFxyXG4gIEFjY29yZGlvbixcclxuICAvL0FuaW1hdGlvblxyXG59O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2pzL21haW4uanMiLCJsZXQgYnJlYWtwb2ludHMgPSB7XHJcbiAgbXM6IDQ4MCxcclxuICBzbTogNzY4LFxyXG4gIG1kOiAxMDI0LFxyXG4gIGxnOiAxMjgwLFxyXG4gIHhsOiAxNjAwXHJcbn07XHJcblxyXG5mdW5jdGlvbiBpc1BvcnRyYWl0KCkge1xyXG4gIHJldHVybiAoJCh3aW5kb3cpLndpZHRoKCkgPCAkKHdpbmRvdykuaGVpZ2h0KCkpO1xyXG59XHJcbmZ1bmN0aW9uIGlzTGFuZHNjYXBlKCkge1xyXG4gIHJldHVybiAoJCh3aW5kb3cpLndpZHRoKCkgPiAkKHdpbmRvdykuaGVpZ2h0KCkpO1xyXG59XHJcbmZ1bmN0aW9uIGlzTW9iaWxlKCl7XHJcbiAgcmV0dXJuICgkKHdpbmRvdykud2lkdGgoKSA8PSBicmVha3BvaW50cy5zbSk7XHJcbn1cclxuZnVuY3Rpb24gaXNNb2JpbGVNUygpe1xyXG4gIHJldHVybiAoJCh3aW5kb3cpLndpZHRoKCkgPD0gYnJlYWtwb2ludHMubXMpO1xyXG59XHJcbmZ1bmN0aW9uIGlzVGFibGV0KCl7XHJcbiAgcmV0dXJuICgkKHdpbmRvdykud2lkdGgoKSA+IGJyZWFrcG9pbnRzLnNtICYmICQod2luZG93KS53aWR0aCgpIDw9IGJyZWFrcG9pbnRzLm1kKVxyXG59XHJcbmZ1bmN0aW9uIGlzRGVza3RvcEV4dCgpe1xyXG4gIHJldHVybiAoJCh3aW5kb3cpLndpZHRoKCkgPj0gYnJlYWtwb2ludHMubWQpXHJcbn1cclxuZnVuY3Rpb24gaXNEZXNrdG9wKCl7XHJcbiAgcmV0dXJuICgkKHdpbmRvdykud2lkdGgoKSA+IGJyZWFrcG9pbnRzLm1kKVxyXG59XHJcbmZ1bmN0aW9uIGlzVG91Y2goKXtcclxuICByZXR1cm4gJ29udG91Y2hzdGFydCcgaW4gd2luZG93IHx8IG5hdmlnYXRvci5tYXhUb3VjaFBvaW50cztcclxufVxyXG5mdW5jdGlvbiBpc01vYmlsZVZlcnNpb24oKXtcclxuICByZXR1cm4gISF+d2luZG93LmxvY2F0aW9uLmhyZWYuaW5kZXhPZihcIi9tb2JpbGUvXCIpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBydW4oKXtcclxuICBpZihpc1RvdWNoKCkpe1xyXG4gICAgJCgnaHRtbCcpLnJlbW92ZUNsYXNzKCduby10b3VjaCcpLmFkZENsYXNzKCd0b3VjaCcpO1xyXG4gIH0gZWxzZSB7XHJcbiAgICAkKCdodG1sJykucmVtb3ZlQ2xhc3MoJ3RvdWNoJykuYWRkQ2xhc3MoJ25vLXRvdWNoJyk7XHJcbiAgfVxyXG59XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IHtcclxuICBydW4sXHJcbiAgaXNUb3VjaCxcclxuICBpc01vYmlsZSxcclxuICBpc01vYmlsZU1TLFxyXG4gIGlzVGFibGV0LFxyXG4gIGlzRGVza3RvcCxcclxuICBpc0Rlc2t0b3BFeHQsXHJcbiAgaXNNb2JpbGVWZXJzaW9uLFxyXG4gIGlzUG9ydHJhaXQsXHJcbiAgaXNMYW5kc2NhcGVcclxufTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2pzL2NvbXBvbmVudHMvZGV2aWNlLWRldGVjdGlvbi5qcyIsIi8qKlxyXG4gKiBIZWxwZXJzXHJcbiAqIEBtb2R1bGUgSGVscGVyc1xyXG4gKi9cclxuXHJcbi8vIEFkZCBzY3JpcHQgYXN5bmhcclxuZnVuY3Rpb24gYWRkU2NyaXB0ICh1cmwpIHtcclxuICB2YXIgdGFnID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInNjcmlwdFwiKTtcclxuICB0YWcuc3JjID0gdXJsO1xyXG4gIHZhciBmaXJzdFNjcmlwdFRhZyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwic2NyaXB0XCIpWzBdO1xyXG4gIGZpcnN0U2NyaXB0VGFnLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKHRhZywgZmlyc3RTY3JpcHRUYWcpO1xyXG59XHJcblxyXG4oZnVuY3Rpb24oKSB7XHJcblxyXG4gIC8vINC/0YDQvtCy0LXRgNGP0LXQvCDQv9C+0LTQtNC10YDQttC60YNcclxuICBpZiAoIUVsZW1lbnQucHJvdG90eXBlLmNsb3Nlc3QpIHtcclxuXHJcbiAgICAvLyDRgNC10LDQu9C40LfRg9C10LxcclxuICAgIEVsZW1lbnQucHJvdG90eXBlLmNsb3Nlc3QgPSBmdW5jdGlvbihjc3MpIHtcclxuICAgICAgdmFyIG5vZGUgPSB0aGlzO1xyXG5cclxuICAgICAgd2hpbGUgKG5vZGUpIHtcclxuICAgICAgICBpZiAobm9kZS5tYXRjaGVzKGNzcykpIHJldHVybiBub2RlO1xyXG4gICAgICAgIGVsc2Ugbm9kZSA9IG5vZGUucGFyZW50RWxlbWVudDtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH07XHJcbiAgfVxyXG5cclxufSkoKTtcclxuXHJcbihmdW5jdGlvbigpIHtcclxuXHJcbiAgLy8g0L/RgNC+0LLQtdGA0Y/QtdC8INC/0L7QtNC00LXRgNC20LrRg1xyXG4gIGlmICghRWxlbWVudC5wcm90b3R5cGUubWF0Y2hlcykge1xyXG5cclxuICAgIC8vINC+0L/RgNC10LTQtdC70Y/QtdC8INGB0LLQvtC50YHRgtCy0L5cclxuICAgIEVsZW1lbnQucHJvdG90eXBlLm1hdGNoZXMgPSBFbGVtZW50LnByb3RvdHlwZS5tYXRjaGVzU2VsZWN0b3IgfHxcclxuICAgICAgRWxlbWVudC5wcm90b3R5cGUud2Via2l0TWF0Y2hlc1NlbGVjdG9yIHx8XHJcbiAgICAgIEVsZW1lbnQucHJvdG90eXBlLm1vek1hdGNoZXNTZWxlY3RvciB8fFxyXG4gICAgICBFbGVtZW50LnByb3RvdHlwZS5tc01hdGNoZXNTZWxlY3RvcjtcclxuXHJcbiAgfVxyXG5cclxufSkoKTtcclxuXHJcbi8qKlxyXG4gKiBDYWxjdWxhdGUgc2Nyb2xsYmFyIHdpZHRoIGluIGVsZW1lbnRcclxuICogLSBpZiB0aGUgd2lkdGggaXMgMCBpdCBtZWFucyB0aGUgc2Nyb2xsYmFyIGlzIGZsb2F0ZWQvb3ZlcmxheWVkXHJcbiAqIC0gYWNjZXB0cyBcImNvbnRhaW5lclwiIHBhcmVtZXRlciBiZWNhdXNlIGllICYgZWRnZSBjYW4gaGF2ZSBkaWZmZXJlbnRcclxuICogICBzY3JvbGxiYXIgYmVoYXZpb3JzIGZvciBkaWZmZXJlbnQgZWxlbWVudHMgdXNpbmcgJy1tcy1vdmVyZmxvdy1zdHlsZSdcclxuICovXHJcbmZ1bmN0aW9uIGdldE5hdGl2ZVNjcm9sbGJhcldpZHRoIChjb250YWluZXIpIHtcclxuICBjb250YWluZXIgPSBjb250YWluZXIgfHwgZG9jdW1lbnQuYm9keTtcclxuXHJcbiAgbGV0IGZ1bGxXaWR0aCA9IDA7XHJcbiAgbGV0IGJhcldpZHRoID0gMDtcclxuXHJcbiAgbGV0IHdyYXBwZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuICBsZXQgY2hpbGQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuXHJcbiAgd3JhcHBlci5zdHlsZS5wb3NpdGlvbiA9ICdhYnNvbHV0ZSc7XHJcbiAgd3JhcHBlci5zdHlsZS5wb2ludGVyRXZlbnRzID0gJ25vbmUnO1xyXG4gIHdyYXBwZXIuc3R5bGUuYm90dG9tID0gJzAnO1xyXG4gIHdyYXBwZXIuc3R5bGUucmlnaHQgPSAnMCc7XHJcbiAgd3JhcHBlci5zdHlsZS53aWR0aCA9ICcxMDBweCc7XHJcbiAgd3JhcHBlci5zdHlsZS5vdmVyZmxvdyA9ICdoaWRkZW4nO1xyXG5cclxuICB3cmFwcGVyLmFwcGVuZENoaWxkKGNoaWxkKTtcclxuICBjb250YWluZXIuYXBwZW5kQ2hpbGQod3JhcHBlcik7XHJcblxyXG4gIGZ1bGxXaWR0aCA9IGNoaWxkLm9mZnNldFdpZHRoO1xyXG4gIHdyYXBwZXIuc3R5bGUub3ZlcmZsb3dZID0gJ3Njcm9sbCc7XHJcbiAgYmFyV2lkdGggPSBmdWxsV2lkdGggLSBjaGlsZC5vZmZzZXRXaWR0aDtcclxuXHJcbiAgY29udGFpbmVyLnJlbW92ZUNoaWxkKHdyYXBwZXIpO1xyXG5cclxuICByZXR1cm4gYmFyV2lkdGg7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUaHJvdHRsZSBIZWxwZXJcclxuICogaHR0cHM6Ly9yZW15c2hhcnAuY29tLzIwMTAvMDcvMjEvdGhyb3R0bGluZy1mdW5jdGlvbi1jYWxsc1xyXG4gKi9cclxuZnVuY3Rpb24gdGhyb3R0bGUgKGZuLCB0aHJlc2hob2xkLCBzY29wZSkge1xyXG4gIHRocmVzaGhvbGQgfHwgKHRocmVzaGhvbGQgPSAyNTApO1xyXG4gIGxldCBsYXN0LFxyXG4gICAgZGVmZXJUaW1lcjtcclxuICByZXR1cm4gZnVuY3Rpb24gKCkge1xyXG4gICAgbGV0IGNvbnRleHQgPSBzY29wZSB8fCB0aGlzO1xyXG5cclxuICAgIGxldCBub3cgPSArbmV3IERhdGUoKSxcclxuICAgICAgYXJncyA9IGFyZ3VtZW50cztcclxuICAgIGlmIChsYXN0ICYmIG5vdyA8IGxhc3QgKyB0aHJlc2hob2xkKSB7XHJcbiAgICAgIC8vIGhvbGQgb24gdG8gaXRcclxuICAgICAgY2xlYXJUaW1lb3V0KGRlZmVyVGltZXIpO1xyXG4gICAgICBkZWZlclRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgbGFzdCA9IG5vdztcclxuICAgICAgICBmbi5hcHBseShjb250ZXh0LCBhcmdzKTtcclxuICAgICAgfSwgdGhyZXNoaG9sZCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBsYXN0ID0gbm93O1xyXG4gICAgICBmbi5hcHBseShjb250ZXh0LCBhcmdzKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBEZWJvdW5jZSBIZWxwZXJcclxuICogaHR0cHM6Ly9yZW15c2hhcnAuY29tLzIwMTAvMDcvMjEvdGhyb3R0bGluZy1mdW5jdGlvbi1jYWxsc1xyXG4gKi9cclxuZnVuY3Rpb24gZGVib3VuY2UgKGZuLCBkZWxheSkge1xyXG4gIGxldCB0aW1lciA9IG51bGw7XHJcbiAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcclxuICAgIGxldCBjb250ZXh0ID0gdGhpcywgYXJncyA9IGFyZ3VtZW50cztcclxuICAgIGNsZWFyVGltZW91dCh0aW1lcik7XHJcbiAgICB0aW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICBmbi5hcHBseShjb250ZXh0LCBhcmdzKTtcclxuICAgIH0sIGRlbGF5KTtcclxuICB9O1xyXG59O1xyXG5cclxubGV0IHRpbWVyO1xyXG5sZXQgdGltZW91dCA9IGZhbHNlO1xyXG5sZXQgZGVsdGEgPSAyMDA7XHJcbmZ1bmN0aW9uIHJlc2l6ZUVuZCgpIHtcclxuICBpZiAobmV3IERhdGUoKSAtIHRpbWVyIDwgZGVsdGEpIHtcclxuICAgIHNldFRpbWVvdXQocmVzaXplRW5kLCBkZWx0YSk7XHJcbiAgfSBlbHNlIHtcclxuICAgIHRpbWVvdXQgPSBmYWxzZTtcclxuICAgICQod2luZG93KS50cmlnZ2VyKCdyZXNpemVlbmQnKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRvZ2dsZUNsYXNzSWYoZWwsIGNvbmQsIHRvZ2dsZWRDbGFzcyl7XHJcblx0aWYoY29uZCl7XHJcblx0XHRlbC5hZGRDbGFzcyh0b2dnbGVkQ2xhc3MpO1xyXG5cdH0gZWxzZSB7XHJcblx0XHRlbC5yZW1vdmVDbGFzcyh0b2dnbGVkQ2xhc3MpO1xyXG5cdH1cclxufVxyXG5cclxuLyoqXHJcbiAqINCk0YPQvdC60YbQuNGPINC00L7QsdCw0LLQu9GP0LXRgiDQuiDRjdC70LXQvNC10L3RgtGDINC60LvQsNGB0YEsINC10YHQu9C4INGB0YLRgNCw0L3QuNGG0LAg0L/RgNC+0LrRgNGD0YfQtdC90LAg0LHQvtC70YzRiNC1LCDRh9C10Lwg0L3QsCDRg9C60LDQt9Cw0L3QvdC+0LUg0LfQvdCw0YfQtdC90LjQtSxcclxuICog0Lgg0YPQsdC40YDQsNC10YIg0LrQu9Cw0YHRgSwg0LXRgdC70Lgg0LfQvdCw0YfQtdC90LjQtSDQvNC10L3RjNGI0LVcclxuICogQHBhcmFtIHtvYmplY3R9IGVsIC0g0Y3Qu9C10LzQtdC90YIsINGBINC60L7RgtC+0YDRi9C8INCy0LfQsNC40LzQvtC00LXQudGB0YLQstGD0LXQvFxyXG4gKiBAcGFyYW0ge21peGVkfSBbc2Nyb2xsVmFsdWU9MF0gLSDQt9C90LDRh9C10L3QuNC1INC/0YDQvtC60YDRg9GC0LrQuCwg0L3QsCDQutC+0YLQvtGA0L7QvCDQvNC10L3Rj9C10LwgY3NzLdC60LvQsNGB0YEsINC+0LbQuNC00LDQtdC80L7QtSDQt9C90LDRh9C10L3QuNC1IC0g0YfQuNGB0LvQviDQuNC70Lgg0LrQu9GO0YfQtdCy0L7QtSDRgdC70L7QstC+ICd0aGlzJy4g0JXRgdC70Lgg0L/QtdGA0LXQtNCw0L3QviAndGhpcycsINC/0L7QtNGB0YLQsNCy0LvRj9C10YLRgdGPINC/0L7Qu9C+0LbQtdC90LjQtSBlbC5vZmZzZXQoKS50b3Ag0LzQuNC90YPRgSDQv9C+0LvQvtCy0LjQvdCwINCy0YvRgdC+0YLRiyDRjdC60YDQsNC90LBcclxuICogQHBhcmFtIHtzdHJpbmd9IFt0b2dnbGVkQ2xhc3M9c2Nyb2xsZWRdIC0gY3NzLdC60LvQsNGB0YEsINC60L7RgtC+0YDRi9C5INC/0LXRgNC10LrQu9GO0YfQsNC10LxcclxuICovXHJcbmZ1bmN0aW9uIHRvZ2dsZUVsZW1lbnRDbGFzc09uU2Nyb2xsKGVsLCBzY3JvbGxWYWx1ZSA9IDAsIHRvZ2dsZWRDbGFzcyA9ICdzY3JvbGxlZCcpe1xyXG5cdGlmKGVsLmxlbmd0aCA9PSAwKSB7XHJcblx0XHQvL2NvbnNvbGUuZXJyb3IoXCLQndC10L7QsdGF0L7QtNC40LzQviDQv9C10YDQtdC00LDRgtGMINC+0LHRitC10LrRgiwg0YEg0LrQvtGC0L7RgNGL0Lwg0LLRiyDRhdC+0YLQuNGC0LUg0LLQt9Cw0LjQvNC+0LTQtdC50YHRgtCy0L7QstCw0YLRjFwiKTtcclxuXHRcdHJldHVybiBmYWxzZTtcclxuXHR9XHJcblxyXG5cdGlmKHNjcm9sbFZhbHVlID09ICd0aGlzJykge1xyXG5cdFx0c2Nyb2xsVmFsdWUgPSBlbC5vZmZzZXQoKS50b3AgLSAkKHdpbmRvdykub3V0ZXJIZWlnaHQoKSAvIDI7XHJcblx0fVxyXG5cclxuXHQkKHdpbmRvdykub24oJ3Njcm9sbCcsIGZ1bmN0aW9uKGUpe1xyXG5cdFx0aWYoJCh3aW5kb3cpLnNjcm9sbFRvcCgpID4gc2Nyb2xsVmFsdWUpe1xyXG5cdFx0XHRlbC5hZGRDbGFzcyh0b2dnbGVkQ2xhc3MpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0ZWwucmVtb3ZlQ2xhc3ModG9nZ2xlZENsYXNzKTtcclxuXHRcdH1cclxuXHR9KTtcclxufTtcclxuXHJcbi8qIE1vZGFscyAqL1xyXG5mdW5jdGlvbiBvcGVuTW9kYWwobW9kYWwpIHtcclxuICBpZiAobW9kYWwpIHtcclxuICAgIGxldCB3aW4gPSBtb2RhbC5maW5kKCcubW9kYWxfX3dpbmRvdycpO1xyXG4gICAgbW9kYWwuZmFkZUluKDUwMCk7XHJcbiAgICAkKCdodG1sLCBib2R5JykuY3NzKCdvdmVyZmxvdy15JywgJ2hpZGRlbicpO1xyXG4gICAgd2luLmZhZGVJbig1MDApO1xyXG4gICAgbW9kYWwudHJpZ2dlcignbW9kYWxvcGVuZWQnKTtcclxuICB9IGVsc2Uge1xyXG4gICAgY29uc29sZS5lcnJvcignV2hpY2ggbW9kYWw/Jyk7XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBjbG9zZU1vZGFsKG1vZGFsKSB7XHJcbiAgaWYgKG1vZGFsKSB7XHJcbiAgICBsZXQgd2luID0gbW9kYWwuZmluZCgnLm1vZGFsX193aW5kb3cnKTtcclxuICAgIHdpbi5mYWRlT3V0KDUwMCk7XHJcbiAgICBtb2RhbC5mYWRlT3V0KDUwMCk7XHJcbiAgICAkKCdodG1sLCBib2R5JykuY3NzKCdvdmVyZmxvdy15JywgJycpO1xyXG4gICAgbW9kYWwudHJpZ2dlcignbW9kYWxjbG9zZWQnKVxyXG4gIH0gZWxzZSB7XHJcbiAgICBjb25zb2xlLmVycm9yKCdXaGljaCBtb2RhbD8nKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHNldFNjcm9sbHBhZChlbHMpIHtcclxuICBpZiAoJCgnLmxheW91dCcpLm91dGVySGVpZ2h0KCkgPiB3aW5kb3cub3V0ZXJIZWlnaHQgfHwgJCgnLnBhZ2UnKS5oYXNDbGFzcygncGFnZS0taG9tZScpKSB7XHJcbiAgICBlbHMuY3NzKHsncGFkZGluZy1yaWdodCc6IGdldE5hdGl2ZVNjcm9sbGJhcldpZHRoKCkgKyAncHgnfSk7XHJcbiAgfSBlbHNlIHtcclxuICAgIGVscy5jc3MoeydwYWRkaW5nLXJpZ2h0JzogJzBweCd9KTtcclxuICB9XHJcbn1cclxuXHJcbi8qIE1lbnUgKi9cclxuZnVuY3Rpb24gc2hvd01lbnUoKSB7XHJcbiAgJCgnLmJ0bi1tZW51JykuYWRkQ2xhc3MoJ29wZW5lZCcpO1xyXG4gICQoJy5tYWluLW1lbnUnKS5hZGRDbGFzcygnb3BlbmVkJyk7XHJcbiAgJCgnYm9keScpLmFkZENsYXNzKCdtZW51LW9wZW5lZCcpO1xyXG59XHJcbmZ1bmN0aW9uIGhpZGVNZW51KCkge1xyXG4gICQoJy5tYWluLW1lbnUnKS5yZW1vdmVDbGFzcygnb3BlbmVkJyk7XHJcbiAgJCgnYm9keScpLnJlbW92ZUNsYXNzKCdtZW51LW9wZW5lZCcpO1xyXG4gICQoJy5idG4tbWVudScpLnJlbW92ZUNsYXNzKCdvcGVuZWQnKTtcclxufVxyXG5cclxuZnVuY3Rpb24gdGV4dENvdW50ZXIoZmllbGQsIGZpZWxkMiwgbWF4bGltaXQpIHtcclxuICB2YXIgY291bnRmaWVsZCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZmllbGQyKTtcclxuICBpZiAoIGZpZWxkLnZhbHVlLmxlbmd0aCA+IG1heGxpbWl0ICkge1xyXG4gICAgZmllbGQudmFsdWUgPSBmaWVsZC52YWx1ZS5zdWJzdHJpbmcoIDAsIG1heGxpbWl0ICk7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfSBlbHNlIHtcclxuICAgIGNvdW50ZmllbGQuaW5uZXJIVE1MID0gbWF4bGltaXQgLSBmaWVsZC52YWx1ZS5sZW5ndGg7XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBpc0VtYWlsKGVtYWlsKSB7XHJcbiAgdmFyIHJlZ2V4ID0gL14oW2EtekEtWjAtOV8uKy1dKStcXEAoKFthLXpBLVowLTktXSkrXFwuKSsoW2EtekEtWjAtOV17Miw0fSkrJC87XHJcbiAgcmV0dXJuIHJlZ2V4LnRlc3QoZW1haWwpO1xyXG59XHJcblxyXG5mdW5jdGlvbiByZWFkVVJMKGlucHV0KSB7XHJcbiAgaWYgKGlucHV0LmZpbGVzICYmIGlucHV0LmZpbGVzWzBdKSB7XHJcbiAgICB2YXIgZmlsZXNBbW91bnQgPSBpbnB1dC5maWxlcy5sZW5ndGg7XHJcblxyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBmaWxlc0Ftb3VudDsgaSsrKSB7XHJcbiAgICAgIHZhciByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG5cclxuICAgICAgcmVhZGVyLm9ubG9hZCA9IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICB2YXIgbGFiZWwgPSAkKCcuanMtaW1nLWFkZCcpLnBhcmVudCgpO1xyXG4gICAgICAgIHZhciBpbWdCbG9jayA9ICc8ZGl2IGNsYXNzPVwiZmllbGRfX3ByZXZpZXdcIiBzdHlsZT1cImJhY2tncm91bmQ6IHVybCgnK2UudGFyZ2V0LnJlc3VsdCsnKTsgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcIj48L2Rpdj4nO1xyXG4gICAgICAgIGxhYmVsLmJlZm9yZSgkKGltZ0Jsb2NrKSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJlYWRlci5yZWFkQXNEYXRhVVJMKGlucHV0LmZpbGVzW2ldKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcblxyXG4vKipcclxuICog0LjQvdC40YbQuNCw0LvQuNC30LDRhtC40Y8g0YHQvtCx0YvRgtC40Lkg0LTQu9GPINC/0LXRgNC10LrQu9GO0YfQsNGC0LXQu9C10Lkg0LrQu9Cw0YHRgdC+0LJcclxuICogQGV4YW1wbGVcclxuICogSGVscGVycy5pbml0KCk7XHJcbiAqL1xyXG5mdW5jdGlvbiBpbml0KCl7XHJcbiAgJChcIi5qcy1pbWctYWRkXCIpLmNoYW5nZShmdW5jdGlvbigpIHtcclxuICAgIHJlYWRVUkwodGhpcyk7XHJcbiAgfSk7XHJcblxyXG4gIC8vSW5wdXRtYXNrKCkubWFzaygnaW5wdXRbbmFtZT1cImJpcnRoZGF5XCJdJyk7XHJcblxyXG4gIC8vIGxldCB0ZXh0YXJlYUVsID0gJy5qcy1mb3JtICNzZW5kX2xldHRlci1wb3B1cC1sZXR0ZXInO1xyXG4gIC8vICQodGV4dGFyZWFFbCkub24oJ2lucHV0JywgZnVuY3Rpb24gKCkge1xyXG4gIC8vICAgdGV4dENvdW50ZXIoZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0ZXh0YXJlYUVsKSwgJy5qcy1zeW1ib2wtbGVmdCcsIDEwMDApO1xyXG4gIC8vIH0pO1xyXG5cclxuICB0b2dnbGVFbGVtZW50Q2xhc3NPblNjcm9sbCgkKCcuaGVhZGVyJyksIDUwKTtcclxuXHJcbiAgJCgnLmJ0bi1tZW51Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSl7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBpZiAoJCh0aGlzKS5oYXNDbGFzcygnb3BlbmVkJykpIHtcclxuICAgICAgaGlkZU1lbnUoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNob3dNZW51KCk7XHJcbiAgICB9XHJcbiAgfSk7XHJcblxyXG4gICQoJy5qcy10b2dnbGUtYmxvY2snKS5vbignY2xpY2snLCBmdW5jdGlvbigpe1xyXG4gICAgbGV0IHRhcmdldCA9ICQodGhpcykuZGF0YSgndGFyZ2V0JykgPT09ICdzZWxmJyA/ICQodGhpcykucGFyZW50KCkgOiAkKCQodGhpcykuZGF0YSgndGFyZ2V0JykpO1xyXG4gICAgaWYgKHRhcmdldC5oYXNDbGFzcygnb2ZmJykpIHtcclxuICAgICAgdGFyZ2V0LnJlbW92ZUNsYXNzKCdvZmYnKS5mYWRlSW4oNTAwKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRhcmdldC5hZGRDbGFzcygnb2ZmJykuZmFkZU91dCg1MDApO1xyXG4gICAgfVxyXG4gIH0pO1xyXG5cclxuICBsZXQgaG92ZXJUb2dnbGVFdmVudCA9ICdtb3VzZWVudGVyIG1vdXNlbGVhdmUnO1xyXG4gIGlmIChNYWluLkRldmljZURldGVjdGlvbi5pc1RvdWNoKCkpIHtcclxuICAgIGhvdmVyVG9nZ2xlRXZlbnQgPSAnY2xpY2snO1xyXG4gIH1cclxuICAkKCcuanMtdG9nZ2xlLWhvdmVyLWJsb2NrJykub24oaG92ZXJUb2dnbGVFdmVudCwgZnVuY3Rpb24oKXtcclxuICAgIGxldCB0YXJnZXQgPSAkKHRoaXMpLmRhdGEoJ3RhcmdldCcpID09PSAnc2VsZicgPyAkKHRoaXMpLnBhcmVudCgpIDogJCgkKHRoaXMpLmRhdGEoJ3RhcmdldCcpKTtcclxuICAgIGlmICh0YXJnZXQuaGFzQ2xhc3MoJ29mZicpKSB7XHJcbiAgICAgIHRhcmdldC5yZW1vdmVDbGFzcygnb2ZmJykuZmFkZUluKDUwMCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAoaG92ZXJUb2dnbGVFdmVudCA9PT0gJ2NsaWNrJykge1xyXG4gICAgICAgIHRhcmdldC5hZGRDbGFzcygnb2ZmJykuZmFkZU91dCg1MDApO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRhcmdldC5vbignbW91c2VsZWF2ZScsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgdGFyZ2V0LmFkZENsYXNzKCdvZmYnKS5mYWRlT3V0KDUwMCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9KTtcclxuXHJcbiAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJy5idG4tY2xvc2UtbW9kYWwnLCBmdW5jdGlvbigpe1xyXG4gICAgbGV0IG1vZGFsID0gISEkKHRoaXMpLmRhdGEoJ3RhcmdldCcpID8gJCgkKHRoaXMpLmRhdGEoJ3RhcmdldCcpKSA6ICQodGhpcykuY2xvc2VzdCgnLm1vZGFsJyk7XHJcbiAgICBjbG9zZU1vZGFsKG1vZGFsKTtcclxuICB9KTtcclxuXHJcbiAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJy5tb2RhbCcsIGZ1bmN0aW9uKCkge1xyXG4gICAgY2xvc2VNb2RhbCgkKHRoaXMpKTtcclxuICB9KTtcclxuXHJcbiAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJy5tb2RhbF9fd2luZG93JywgZnVuY3Rpb24oZSkge1xyXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICB9KTtcclxuXHJcbiAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJy5idG4tbW9kYWwnLCBmdW5jdGlvbihlKSB7XHJcbiAgICBsZXQgdGFyZ2V0ID0gJCh0aGlzKS5kYXRhKCd0YXJnZXQnKSA9PT0gJ3NlbGYnID8gJCh0aGlzKS5wYXJlbnQoKSA6ICQoJCh0aGlzKS5kYXRhKCd0YXJnZXQnKSk7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBvcGVuTW9kYWwodGFyZ2V0KTtcclxuICB9KTtcclxuXHJcbiAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJy5qcy1sZXR0ZXItc2hhcmUnLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgJCh0aGlzKS5wYXJlbnQoKS5wYXJlbnQoKS50b2dnbGVDbGFzcygnX3NoYXJlJyk7XHJcbiAgfSk7XHJcblxyXG4gIC8vICQod2luZG93KS5vbigncmVzaXplJywgZnVuY3Rpb24gKCkge1xyXG4gIC8vICAgdGltZXIgPSBuZXcgRGF0ZSgpO1xyXG4gIC8vICAgaWYgKHRpbWVvdXQgPT09IGZhbHNlKSB7XHJcbiAgLy8gICAgIHRpbWVvdXQgPSB0cnVlO1xyXG4gIC8vICAgICBzZXRUaW1lb3V0KHJlc2l6ZUVuZCwgZGVsdGEpO1xyXG4gIC8vICAgfVxyXG4gIC8vIH0pO1xyXG5cclxuICAvL3NldFNjcm9sbHBhZCgkKCcubGF5b3V0LCAuaGVhZGVyJykpO1xyXG5cclxuICAvLyAkKHdpbmRvdykub24oJ3Jlc2l6ZWVuZCcsIGZ1bmN0aW9uKCl7XHJcbiAgLy8gICBzZXRTY3JvbGxwYWQoJCgnLmxheW91dCwgLmhlYWRlcicpKTtcclxuICAvLyB9KTtcclxuXHJcbiAgLy8gJCh3aW5kb3cpLm9uKCdyZXNpemVlbmQnLCBmdW5jdGlvbigpe1xyXG4gIC8vICAgaWYgKE1haW4uRGV2aWNlRGV0ZWN0aW9uLmlzTW9iaWxlKCkpIHtcclxuICAvLyAgICAgaGlkZU1lbnUoKTtcclxuICAvLyAgIH0gZWxzZSB7XHJcbiAgLy8gICAgIHNob3dNZW51KCk7XHJcbiAgLy8gICB9XHJcbiAgLy8gfSk7XHJcblxyXG4gIC8qXHJcbiAgaWYgKE1haW4uRGV2aWNlRGV0ZWN0aW9uLmlzUG9ydHJhaXQoKSkge1xyXG4gICAgJCgnaHRtbCcpLmFkZENsYXNzKCdyb3RhdGVkJyk7XHJcbiAgICAkKCcucm90YXRlJykuZmFkZUluKDUwMCk7XHJcbiAgfVxyXG5cclxuICAkKHdpbmRvdykub24oJ3Jlc2l6ZWVuZCcsIGZ1bmN0aW9uKCl7XHJcbiAgICBpZiAoTWFpbi5EZXZpY2VEZXRlY3Rpb24uaXNQb3J0cmFpdCgpKSB7XHJcbiAgICAgICQoJ2h0bWwnKS5hZGRDbGFzcygncm90YXRlZCcpO1xyXG4gICAgICAkKCcucm90YXRlJykuZmFkZUluKDUwMCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAkKCcucm90YXRlJykuZmFkZU91dCg1MDApO1xyXG4gICAgICAkKCdodG1sJykucmVtb3ZlQ2xhc3MoJ3JvdGF0ZWQnKTtcclxuICAgIH1cclxuICB9KTtcclxuICAqL1xyXG5cclxuICAkKCcuYnRuLWFjY29yZGlvbi1tZW51Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgJCh0aGlzKS5jbG9zZXN0KCcubXVsdGknKS50b2dnbGVDbGFzcygnb3BlbicpO1xyXG4gICAgJCh0aGlzKS5zaWJsaW5ncygnLnN1Ym5hdicpLnNsaWRlVG9nZ2xlKDUwMCk7XHJcbiAgfSlcclxuXHJcbiAgJCgnLmZlZWRiYWNrJykuZmluZCgnaW5wdXRbdHlwZT1cImZpbGVcIl0nKS5vbignY2hhbmdlJywgZnVuY3Rpb24gKCkge1xyXG4gICAgbGV0IGZpID0gJCh0aGlzKS5nZXQoMCk7XHJcbiAgICBsZXQgZm9ybSA9ICQodGhpcykucGFyZW50cygnZm9ybScpO1xyXG4gICAgaWYgKGZpLmZpbGVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPD0gZmkuZmlsZXMubGVuZ3RoIC0gMTsgaSsrKSB7XHJcbiAgICAgICAgdmFyIGZ1bGxQYXRoID0gJCh0aGlzKS52YWwoKTtcclxuICAgICAgICB2YXIgcGF0aEVsID0gJCgnLmpzLWZpbGUtcGF0aCcpO1xyXG4gICAgICAgIGlmIChmdWxsUGF0aCkge1xyXG4gICAgICAgICAgICB2YXIgc3RhcnRJbmRleCA9IChmdWxsUGF0aC5pbmRleE9mKCdcXFxcJykgPj0gMCA/IGZ1bGxQYXRoLmxhc3RJbmRleE9mKCdcXFxcJykgOiBmdWxsUGF0aC5sYXN0SW5kZXhPZignLycpKTtcclxuICAgICAgICAgICAgdmFyIGZpbGVuYW1lID0gZnVsbFBhdGguc3Vic3RyaW5nKHN0YXJ0SW5kZXgpO1xyXG4gICAgICAgICAgICBpZiAoZmlsZW5hbWUuaW5kZXhPZignXFxcXCcpID09PSAwIHx8IGZpbGVuYW1lLmluZGV4T2YoJy8nKSA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgZmlsZW5hbWUgPSBmaWxlbmFtZS5zdWJzdHJpbmcoMSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgJCh0aGlzKS5wYXJlbnRzKCcuZmllbGQnKS5hZGRDbGFzcygnX3Nob3ctcGF0aCcpO1xyXG4gICAgICAgICAgICBwYXRoRWwuaHRtbChmaWxlbmFtZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSk7XHJcblxyXG4gIGlmICgkKHdpbmRvdykud2lkdGgoKSA+IDExOTkpIHtcclxuICAgICQoJy5qcy1zdWJtZW51Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICQodGhpcykudG9nZ2xlQ2xhc3MoJ29wZW5lZCcpO1xyXG4gICAgfSk7XHJcbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XHJcbiAgICAgIGlmICghJChlLnRhcmdldCkuaXMoJy5tYWluLW1lbnVfX25hbWUsIC5tYWluLW1lbnVfX2F2YXRhciwgLmpzLXN1Ym1lbnUnKSkge1xyXG4gICAgICAgICQoJy5qcy1zdWJtZW51JykucmVtb3ZlQ2xhc3MoJ29wZW5lZCcpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8vICQod2luZG93KS5zY3JvbGwoJC5kZWJvdW5jZSgyNTAsIHRydWUsIGZ1bmN0aW9uKCkge1xyXG4gIC8vICAgJCgnaHRtbCcpLmFkZENsYXNzKCdpcy1zY3JvbGxpbmcnKTtcclxuICAvLyB9KSk7XHJcbiAgLy8gJCh3aW5kb3cpLnNjcm9sbCgkLmRlYm91bmNlKDI1MCwgZnVuY3Rpb24oKSB7XHJcbiAgLy8gICAkKCdodG1sJykucmVtb3ZlQ2xhc3MoJ2lzLXNjcm9sbGluZycpO1xyXG4gIC8vIH0pKTtcclxuXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdldFBhcmFtZXRlckJ5TmFtZShuYW1lLCB1cmwpIHtcclxuICBpZiAoIXVybCkgdXJsID0gd2luZG93LmxvY2F0aW9uLmhyZWY7XHJcbiAgbmFtZSA9IG5hbWUucmVwbGFjZSgvW1xcW1xcXV0vZywgJ1xcXFwkJicpO1xyXG4gIHZhciByZWdleCA9IG5ldyBSZWdFeHAoJ1s/Jl0nICsgbmFtZSArICcoPShbXiYjXSopfCZ8I3wkKScpLFxyXG4gICAgICByZXN1bHRzID0gcmVnZXguZXhlYyh1cmwpO1xyXG4gIGlmICghcmVzdWx0cykgcmV0dXJuIG51bGw7XHJcbiAgaWYgKCFyZXN1bHRzWzJdKSByZXR1cm4gJyc7XHJcbiAgcmV0dXJuIGRlY29kZVVSSUNvbXBvbmVudChyZXN1bHRzWzJdLnJlcGxhY2UoL1xcKy9nLCAnICcpKTtcclxufVxyXG5cclxuZnVuY3Rpb24gdHJpbVRleHQoZWwpIHtcclxuICB2YXIgd3JhcCA9ICQoJ1tkYXRhLXRhcmdldD1cIicrZWwrJ1wiXScpLnBhcmVudHMoJy5sZXR0ZXItLXRleHQnKTtcclxuICB2YXIgcCA9IHdyYXAuZmluZCgnLmpzLWxldHRlci1wcmV2aWV3LXRleHQtcCcpO1xyXG4gIHZhciBkaXZoID0gd3JhcC5maW5kKCcuanMtbGV0dGVyLXByZXZpZXctdGV4dCcpLmhlaWdodCgpO1xyXG5cclxuICBpZiAocC5vdXRlckhlaWdodCgpID4gZGl2aCkge1xyXG4gICAgICBwLnRleHQoZnVuY3Rpb24gKGluZGV4LCB0ZXh0KSB7XHJcbiAgICAgICAgICByZXR1cm4gdGV4dC5yZXBsYWNlKC9cXFcqXFxzKFxcUykqJC8sICcuLi4nKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiB6ZXJvUGFkKG5yLGJhc2Upe1xyXG4gIHZhciAgbGVuID0gKFN0cmluZyhiYXNlKS5sZW5ndGggLSBTdHJpbmcobnIpLmxlbmd0aCkrMTtcclxuICByZXR1cm4gbGVuID4gMD8gbmV3IEFycmF5KGxlbikuam9pbignMCcpK25yIDogbnI7XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0ge1xyXG4gIGluaXQsXHJcbiAgZ2V0TmF0aXZlU2Nyb2xsYmFyV2lkdGgsXHJcbiAgdG9nZ2xlQ2xhc3NJZixcclxuICB0b2dnbGVFbGVtZW50Q2xhc3NPblNjcm9sbCxcclxuICBhZGRTY3JpcHQsXHJcbiAgb3Blbk1vZGFsLFxyXG4gIGNsb3NlTW9kYWwsXHJcbiAgc2hvd01lbnUsXHJcbiAgaGlkZU1lbnUsXHJcbiAgemVyb1BhZCxcclxuICB0cmltVGV4dCxcclxuICBnZXRQYXJhbWV0ZXJCeU5hbWUsXHJcbiAgaXNFbWFpbFxyXG59O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2pzL2NvbXBvbmVudHMvaGVscGVycy5qcyIsIi8qKlxyXG4gKiDQn9C10YDQtdC60LvRjtGH0LXQvdC40LUg0LrQu9Cw0YHRgdC+0LIg0L/QviDRgNCw0LfQu9C40YfQvdGL0Lwg0YHQvtCx0YvRgtC40Y/QvFxyXG4gKiBAbW9kdWxlIFBvcHVwc1xyXG4gKi9cclxuXHJcbi8qIFBvcHVwcyAqL1xyXG5mdW5jdGlvbiBvcGVuUG9wdXAocG9wdXBJRCkge1xyXG4gIGNsb3NlQWxsUG9wdXBzKCk7XHJcbiAgaWYgKHBvcHVwSUQpIHtcclxuICAgIGxldCBwb3B1cCA9ICQocG9wdXBJRCk7XHJcbiAgICBsZXQgd2luID0gcG9wdXAuZmluZCgnLnBvcHVwX193aW5kb3cnKTtcclxuICAgIHBvcHVwLmZhZGVJbig1MDApO1xyXG4gICAgaWYgKE1haW4uRGV2aWNlRGV0ZWN0aW9uLmlzUG9ydHJhaXQoKSkge1xyXG4gICAgICAkKCdodG1sLCBib2R5JykuY3NzKCdvdmVyZmxvdycsICdoaWRkZW4nKTtcclxuICAgIH1cclxuICAgICQoJ2h0bWwnKS5hZGRDbGFzcygncG9wdXAtb3BlbmVkJyk7XHJcbiAgICB3aW4uZmFkZUluKDUwMCk7XHJcbiAgICBwb3B1cC50cmlnZ2VyKCdwb3B1cG9wZW5lZCcpO1xyXG4gICAgJCh3aW5kb3cpLnRyaWdnZXIoJ3BvcHVwb3BlbmVkJyk7XHJcblxyXG4gICAgaWYgKHBvcHVwLmF0dHIoJ2lkJykgPT0gJ2Nyb3AtcG9wdXAnKSB7XHJcbiAgICAgICQod2luZG93KS50cmlnZ2VyKCdjcm9wLXBvcHVwb3BlbmVkJyk7XHJcbiAgICB9XHJcbiAgfSBlbHNlIHtcclxuICAgIGNvbnNvbGUuZXJyb3IoJ1doaWNoIHBvcHVwPycpO1xyXG4gIH1cclxufVxyXG5cclxuZnVuY3Rpb24gY2xvc2VQb3B1cChwb3B1cElEKSB7XHJcbiAgaWYgKHBvcHVwSUQpIHtcclxuICAgIGxldCBwb3B1cCA9ICQocG9wdXBJRCk7XHJcbiAgICBsZXQgd2luID0gcG9wdXAuZmluZCgnLnBvcHVwX193aW5kb3cnKTtcclxuICAgIHdpbi5mYWRlT3V0KDUwMCk7XHJcbiAgICBwb3B1cC5mYWRlT3V0KDUwMCk7XHJcbiAgICAkKCdodG1sJykucmVtb3ZlQ2xhc3MoJ3BvcHVwLW9wZW5lZCcpO1xyXG4gICAgaWYgKE1haW4uRGV2aWNlRGV0ZWN0aW9uLmlzUG9ydHJhaXQoKSkge1xyXG4gICAgICAkKCdodG1sLCBib2R5JykuY3NzKCdvdmVyZmxvdycsICd2aXNpYmxlJyk7XHJcbiAgICB9XHJcbiAgICBwb3B1cC50cmlnZ2VyKCdwb3B1cGNsb3NlZCcpXHJcbiAgICAkKHdpbmRvdykudHJpZ2dlcigncG9wdXBjbG9zZWQnKTtcclxuICB9IGVsc2Uge1xyXG4gICAgY29uc29sZS5lcnJvcignV2hpY2ggcG9wdXA/Jyk7XHJcbiAgfVxyXG4gIGlmIChwb3B1cElEID09ICcjcXItcG9wdXAnKSB7XHJcbiAgICAkKHdpbmRvdykudHJpZ2dlcignc2Nhbi1zdG9wJyk7XHJcbiAgfVxyXG4gIC8vTWFpbi5IZWxwZXJzLnJlbW92ZUhhc2goKTtcclxufVxyXG5cclxuZnVuY3Rpb24gY2xvc2VBbGxQb3B1cHMoKSB7XHJcbiAgbGV0IHBvcHVwID0gJCgnLnBvcHVwJyk7XHJcbiAgbGV0IHdpbiA9IHBvcHVwLmZpbmQoJy5wb3B1cF9fd2luZG93Jyk7XHJcbiAgd2luLmZhZGVPdXQoMjAwKTtcclxuICBwb3B1cC5mYWRlT3V0KDIwMCk7XHJcbiAgJCgnaHRtbCcpLnJlbW92ZUNsYXNzKCdwb3B1cC1vcGVuZWQnKTtcclxuICBpZiAoTWFpbi5EZXZpY2VEZXRlY3Rpb24uaXNQb3J0cmFpdCgpKSB7XHJcbiAgICAkKCdodG1sLCBib2R5JykuY3NzKCdvdmVyZmxvdycsICd2aXNpYmxlJyk7XHJcbiAgfVxyXG4gIHBvcHVwLnRyaWdnZXIoJ3BvcHVwY2xvc2VkJyk7XHJcbiAgJCh3aW5kb3cpLnRyaWdnZXIoJ3BvcHVwY2xvc2VkJyk7XHJcbiAgdmFyIGV2ZW50ID0gbmV3IEV2ZW50KCdzY2FuLXN0b3AnKTtcclxuICBkb2N1bWVudC5kaXNwYXRjaEV2ZW50KGV2ZW50KTtcclxuICAvL01haW4uSGVscGVycy5yZW1vdmVIYXNoKCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGluaXQgKCkge1xyXG5cclxuICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnLmJ0bi1jbG9zZS1wb3B1cCcsIGZ1bmN0aW9uKCl7XHJcbiAgICBsZXQgcG9wdXAgPSAhISQodGhpcykuZGF0YSgndGFyZ2V0JykgPyAkKCQodGhpcykuZGF0YSgndGFyZ2V0JykpIDogJCh0aGlzKS5jbG9zZXN0KCcucG9wdXAnKS5hdHRyKCdpZCcpO1xyXG4gICAgY2xvc2VQb3B1cChgIyR7cG9wdXB9YCk7XHJcbiAgfSk7XHJcblxyXG4gICQoZG9jdW1lbnQpLm9uKCdjbGljaycsICcucG9wdXAnLCBmdW5jdGlvbigpIHtcclxuICAgIGlmICghJCh0aGlzKS5oYXNDbGFzcygndW5jbG9zZWQnKSkge1xyXG4gICAgICBjbG9zZVBvcHVwKGAjJHskKHRoaXMpLmF0dHIoJ2lkJyl9YCk7XHJcbiAgICB9XHJcbiAgfSk7XHJcblxyXG4gICQoZG9jdW1lbnQpLm9uKCdjbGljaycsICcucG9wdXBfX3dpbmRvdywgLnBvcHVwIC5tQ1NCX3Njcm9sbFRvb2xzJywgZnVuY3Rpb24oZSkge1xyXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICB9KTtcclxuXHJcbiAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJy5idG4tcG9wdXAnLCBmdW5jdGlvbihlKSB7XHJcbiAgICBsZXQgdGFyZ2V0ID0gJCh0aGlzKS5kYXRhKCd0YXJnZXQnKSA9PT0gJ3NlbGYnID8gJCh0aGlzKS5wYXJlbnQoKSA6ICQoJCh0aGlzKS5kYXRhKCd0YXJnZXQnKSk7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBvcGVuUG9wdXAodGFyZ2V0KTtcclxuICB9KTtcclxuXHJcbiAgJCh3aW5kb3cpLm9uKCdwb3B1cGNsb3NlZCcsIGZ1bmN0aW9uKCkge1xyXG4gICAgJCgndmlkZW8nKS50cmlnZ2VyKCdwYXVzZScpO1xyXG4gIH0pO1xyXG5cclxuICAvLyBhcG0td2lkamV0XHJcbiAgLy8gaHR0cHM6Ly9xci5hcG1jaGVjay5ydS9yZWFkbWVcclxuICBpZiAoIXdpbmRvdy5zeXMpIHtcclxuICAgIHdpbmRvdy5zeXMgPSB7fTtcclxuICAgIHdpbmRvdy5zeXMudXNlclV1aWQgPSBcInRlc3RAbWFpbC5ydVwiO1xyXG4gIH1cclxuICB2YXIgd2lkZ2V0UGFyYW1zID0ge1xyXG4gICAgYXBpOiAnaHR0cHM6Ly9hcGkuYXBtY2hlY2sucnUnLFxyXG4gICAgYXBpS2V5OiAnMDhmYTk0NWItYjkxMS00NWE2LWJhYmUtMjZlNTRmNTdiNGNjJyxcclxuICAgIHVzZXJVdWlkOiB3aW5kb3cuc3lzLnVzZXJVdWlkLFxyXG4gICAgaTE4bjoge1xyXG4gICAgICBsYWJlbHM6IHtcclxuICAgICAgICBtYWluQnV0dG9uOiAn0JfQsNCz0YDRg9C30LjRgtGMINGH0LXQuicsXHJcbiAgICAgICAgbWFudWFsUXJCdXR0b246ICfQktCy0LXRgdGC0Lgg0LTQsNC90L3Ri9C1INCy0YDRg9GH0L3Rg9GOJyxcclxuICAgICAgICB1cGxvYWRQaG90b3NCdXR0b246ICfQl9Cw0LPRgNGD0LfQuNGC0Ywg0YTQvtGC0L4g0YfQtdC60LAnLFxyXG4gICAgICAgIHN1Ym1pdE1hbnVhbFFyQnV0dG9uOiAn0J7RgtC/0YDQsNCy0LjRgtGMJyxcclxuICAgICAgICBhZGRQaG90b0J1dHRvbjogJzxzdmcgY2xhc3M9XCJpY29uXCI+PHVzZSB4bGluazpocmVmPVwiI3VwbG9hZFwiLz48L3N2Zz4gPHNwYW4gY2xhc3M9XCJ0ZXh0XCI+0JfQsNCz0YDRg9C30LjRgtGMINGE0L7RgtC+PC9zcGFuPidcclxuICAgICAgfSxcclxuICAgICAgc2NyZWVuczoge1xyXG4gICAgICAgIGNhbWVyYU5vdEF2YWlsYWJsZToge1xyXG4gICAgICAgICAgaGVhZGVyOiBcItCX0LDQs9GA0YPQt9C60LAg0YfQtdC60LBcIixcclxuICAgICAgICAgIHN1YmhlYWRlcjogJ9Cc0Ysg0L3QtSZuYnNwO9C80L7QttC10Lwg0L/QvtC70YPRh9C40YLRjCDQtNC+0YHRgtGD0L8g0LombmJzcDvQutCw0LzQtdGA0LUmbmJzcDvRg9GB0YLRgNC+0LnRgdGC0LLQsC48YnI+PGJyLz7QoNCw0LfRgNC10YjQuNGC0LUg0LHRgNCw0YPQt9C10YDRgyDQvtCx0YDQsNGJ0LDRgtGM0YHRjyDQuiZuYnNwO9C60LDQvNC10YDQtSDQuNC70Lgg0LLQstC10LTQuNGC0LUg0LTQsNC90L3Ri9C1INGBJm5ic3A70YfQtdC60LAg0LLRgNGD0YfQvdGD0Y4nXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgaWYgKCAkKCcjYXBtLXdpZGdldCcpLmxlbmd0aCApIHtcclxuICAgIHFyV2lkZ2V0LmluaXQoJ2FwbS13aWRnZXQnLCB3aWRnZXRQYXJhbXMpO1xyXG4gICAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJy5qcy1zY2FubmVyLWJ0bicsIGZ1bmN0aW9uIChldikge1xyXG4gICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICB2YXIgZSA9IGRvY3VtZW50LmNyZWF0ZUV2ZW50KCdIVE1MRXZlbnRzJyk7XHJcbiAgICAgIGUuaW5pdEV2ZW50KCdjbGljaycsIHRydWUsIHRydWUpO1xyXG4gICAgICAkKCcjYXBtLXNjYW4tcXInKVswXS5kaXNwYXRjaEV2ZW50KGUpO1xyXG4gICAgfSk7XHJcbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnLmFwbS1idG4tbGluayAnLCBmdW5jdGlvbiAoZXYpIHtcclxuICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG5cclxubW9kdWxlLmV4cG9ydHMgPSB7XHJcbiAgaW5pdCxcclxuICBvcGVuUG9wdXAsXHJcbiAgY2xvc2VQb3B1cCxcclxuICBjbG9zZUFsbFBvcHVwc1xyXG59O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2pzL2NvbXBvbmVudHMvcG9wdXBzLmpzIiwiLyoqXHJcbiAqINCa0LDRgNGD0YHQtdC70YxcclxuICogQG1vZHVsZSBDYXJvdXNlbFxyXG4gKiBodHRwczovL2lkYW5nZXJvLnVzL3N3aXBlci9hcGkvXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqINCY0L3QuNGG0LjQsNC70LjQt9Cw0YbQuNGPINC60LDRgNGD0YHQtdC70LhcclxuICovXHJcbmZ1bmN0aW9uIGluaXQoKXtcclxuICAgIGlmICgkKHdpbmRvdykud2lkdGgoKSA8IDc2OCkge1xyXG4gICAgICBcclxuICAgICAgaWYgKCQoJy5zdGVwcycpLmxlbmd0aCkge1xyXG4gICAgICAgICQoJy5zdGVwcycpLmFkZENsYXNzKCdzd2lwZXItd3JhcHBlcicpO1xyXG5cclxuICAgICAgICB2YXIgU3dpcGVyQ2Fyb3VzZWwgPSBuZXcgU3dpcGVyKCcuc3RlcHMtc2xpZGVyJywge1xyXG4gICAgICAgICAgc2xpZGVzUGVyVmlldzogMSxcclxuICAgICAgICAgIG5hdmlnYXRpb246IHtcclxuICAgICAgICAgICAgbmV4dEVsOiAnLnN0ZXBzLXNsaWRlciAuc3dpcGVyLWJ1dHRvbi1uZXh0JyxcclxuICAgICAgICAgICAgcHJldkVsOiAnLnN0ZXBzLXNsaWRlciAuc3dpcGVyLWJ1dHRvbi1wcmV2JyxcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBwYWdpbmF0aW9uOiB7XHJcbiAgICAgICAgICAgIGVsOiAnLnN0ZXBzLXNsaWRlciAuc3dpcGVyLXBhZ2luYXRpb24nLFxyXG4gICAgICAgICAgICBjbGlja2FibGU6IHRydWVcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKCQoJy5ydWxlcy1wcml6ZXMnKS5sZW5ndGgpIHtcclxuICAgICAgICAkKCcucnVsZXMtcHJpemVzJykuYWRkQ2xhc3MoJ3N3aXBlci13cmFwcGVyJyk7XHJcblxyXG4gICAgICAgIHZhciBTd2lwZXJDYXJvdXNlbCA9IG5ldyBTd2lwZXIoJy5ydWxlcy1zbGlkZXInLCB7XHJcbiAgICAgICAgICBzbGlkZXNQZXJWaWV3OiAxLFxyXG4gICAgICAgICAgbmF2aWdhdGlvbjoge1xyXG4gICAgICAgICAgICBuZXh0RWw6ICcucnVsZXMtc2xpZGVyIC5zd2lwZXItYnV0dG9uLW5leHQnLFxyXG4gICAgICAgICAgICBwcmV2RWw6ICcucnVsZXMtc2xpZGVyIC5zd2lwZXItYnV0dG9uLXByZXYnLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIHBhZ2luYXRpb246IHtcclxuICAgICAgICAgICAgZWw6ICcucnVsZXMtc2xpZGVyIC5zd2lwZXItcGFnaW5hdGlvbicsXHJcbiAgICAgICAgICAgIGNsaWNrYWJsZTogdHJ1ZVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbn07XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IHtpbml0fTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9qcy9jb21wb25lbnRzL2Nhcm91c2VsLmpzIiwiLyoqXHJcbiAqINCf0L7QtNC60LvRjtGH0LXQvdC40LUgU2VsZWN0c1xyXG4gKiBAbW9kdWxlIFNlbGVjdHNcclxuICogaHR0cHM6Ly9zZWxlY3QyLm9yZy9jb25maWd1cmF0aW9uL29wdGlvbnMtYXBpXHJcbiAqL1xyXG5cclxuZnVuY3Rpb24gaW5pdCgpIHtcclxuXHJcbiAgaWYgKCQoJy5qcy1zZWxlY3QnKS5sZW5ndGgpIHtcclxuICAgICQoJy5qcy1zZWxlY3QnKS5zZWxlY3QyKHtcclxuICAgICAgd2lkdGg6ICdzdHlsZScsXHJcbiAgICAgIG1pbmltdW1SZXN1bHRzRm9yU2VhcmNoOiAtMVxyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IHtcclxuICBpbml0XHJcbn1cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9qcy9jb21wb25lbnRzL3NlbGVjdHMuanMiLCIvKipcclxuICog0LjQvdC40YbQuNCw0LvQuNC30LDRhtC40Y8g0YHQvtCx0YvRgtC40Lkg0LTQu9GPINC60LDRgdGC0L7QvNC90L7Qs9C+INGB0LXQu9C10LrRgtCwXHJcbiAqIEBleGFtcGxlXHJcbiAqIFRhYnMuaW5pdCgpO1xyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIGluaXQoKXtcclxuXHJcbiAgJCgnLnRhYnMnKS5lYWNoKGZ1bmN0aW9uKCkge1xyXG4gICAgbGV0IGFjdGl2ZSA9ICQodGhpcykuZmluZCgnLnRhYi1jdHJsLmFjdGl2ZScpLmxlbmd0aCA/ICQodGhpcykuZmluZCgnLnRhYi1jdHJsLmFjdGl2ZScpIDogJCh0aGlzKS5maW5kKCcudGFiLWN0cmwnKS5lcSgwKTtcclxuICAgIGFjdGl2ZS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAkKGFjdGl2ZS5kYXRhKCd0YWInKSkuZmFkZUluKDUwMCkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gIH0pXHJcblxyXG4gICQoJy50YWItY3RybCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XHJcbiAgICBpZiAoISQodGhpcykuaGFzQ2xhc3MoJ2FjdGl2ZScpKSB7XHJcbiAgICAgICQodGhpcykuc2libGluZ3MoJy50YWItY3RybCcpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgJCh0aGlzKS5jbG9zZXN0KCcudGFicycpLmZpbmQoJy50YWIuYWN0aXZlJykuZmFkZU91dCg1MCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAkKHRoaXMpLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgJCgkKHRoaXMpLmRhdGEoJ3RhYicpKS5mYWRlSW4oNTAwKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICB9XHJcbiAgfSk7XHJcblxyXG4gICQoJy5idG4tdGFiLWN0cmwnKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuICAgIGxldCBhY3RpdmUgPSAkKHRoaXMpLmRhdGEoJ3RhYicpO1xyXG4gICAgJCh0aGlzKS5jbG9zZXN0KCcudGFicycpLmZpbmQoJy50YWItY3RybCcpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICQodGhpcykuY2xvc2VzdCgnLnRhYnMnKS5maW5kKCcudGFiLmFjdGl2ZScpLmZhZGVPdXQoNTApLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICQodGhpcykuY2xvc2VzdCgnLnRhYnMnKS5maW5kKGAudGFiLWN0cmxbZGF0YS10YWI9XCIke2FjdGl2ZX1cIl1gKS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAkKGFjdGl2ZSkuZmFkZUluKDUwMCkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gIH0pO1xyXG5cclxuICAkKCcudGFiLXByZXYnKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuICAgIGlmICgkKHRoaXMpLmNsb3Nlc3QoJy50YWJzJykuZmluZCgnLnRhYi1jdHJsLmFjdGl2ZScpLnByZXYoKS5sZW5ndGgpe1xyXG4gICAgICAkKHRoaXMpLmNsb3Nlc3QoJy50YWJzJykuZmluZCgnLnRhYi1jdHJsLmFjdGl2ZScpLnByZXYoKS50cmlnZ2VyKCdjbGljaycpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgJCh0aGlzKS5jbG9zZXN0KCcudGFicycpLmZpbmQoJy50YWItY3RybCcpLmxhc3QoKS50cmlnZ2VyKCdjbGljaycpO1xyXG4gICAgfVxyXG4gIH0pO1xyXG4gICQoJy50YWItbmV4dCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG4gICAgaWYgKCQodGhpcykuY2xvc2VzdCgnLnRhYnMnKS5maW5kKCcudGFiLWN0cmwuYWN0aXZlJykubmV4dCgpLmxlbmd0aCl7XHJcbiAgICAgICQodGhpcykuY2xvc2VzdCgnLnRhYnMnKS5maW5kKCcudGFiLWN0cmwuYWN0aXZlJykubmV4dCgpLnRyaWdnZXIoJ2NsaWNrJyk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAkKHRoaXMpLmNsb3Nlc3QoJy50YWJzJykuZmluZCgnLnRhYi1jdHJsJykuZmlyc3QoKS50cmlnZ2VyKCdjbGljaycpO1xyXG4gICAgfVxyXG4gIH0pO1xyXG59XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IHtpbml0fTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHNyYy9qcy9jb21wb25lbnRzL3RhYnMuanMiLCIvKipcclxuICog0LjQvdC40YbQuNCw0LvQuNC30LDRhtC40Y8g0YHQvtCx0YvRgtC40Lkg0LTQu9GPINC60LDRgdGC0L7QvNC90L7Qs9C+INGB0LXQu9C10LrRgtCwXHJcbiAqIEBleGFtcGxlXHJcbiAqIEFjY29yZGlvbi5pbml0KCk7XHJcbiAqL1xyXG5cclxuZnVuY3Rpb24gaW5pdCgpe1xyXG5cclxuICAkKCcuYWNjb3JkaW9uX19wYW5lbCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XHJcbiAgICAkKHRoaXMpLmNsb3Nlc3QoJy5hY2NvcmRpb24nKS5zaWJsaW5ncygnLmFjY29yZGlvbicpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICQodGhpcykuY2xvc2VzdCgnLmFjY29yZGlvbicpLnNpYmxpbmdzKCcuYWNjb3JkaW9uJykuZmluZCgnLmFjY29yZGlvbl9fY29udGVudCcpLnNsaWRlVXAoNTAwKTtcclxuICAgICQodGhpcykuY2xvc2VzdCgnLmFjY29yZGlvbicpLnRvZ2dsZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICQodGhpcykuc2libGluZ3MoJy5hY2NvcmRpb25fX2NvbnRlbnQnKS5zbGlkZVRvZ2dsZSg1MDApO1xyXG4gIH0pO1xyXG59XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IHtpbml0fTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2pzL2NvbXBvbmVudHMvYWNjb3JkaW9uLmpzIl0sIm1hcHBpbmdzIjoiOztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FDdENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBOzs7Ozs7OztBQzNCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBOzs7Ozs7OztBQzVDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBS0E7QUFFQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWJBOzs7Ozs7OztBQ3hiQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBREE7QUFSQTtBQUpBO0FBQ0E7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTs7Ozs7Ozs7QUNySUE7Ozs7OztBQU1BOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQU5BO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBTkE7QUFXQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FDN0NBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTs7Ozs7Ozs7QUNoQkE7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQzlDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Iiwic291cmNlUm9vdCI6IiJ9